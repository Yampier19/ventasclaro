<!DOCTYPE html>
<html lang="es">
<head>
	<title>Claro Ventas</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../../DESIGN/IMG/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/font/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/font/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/util.css">
	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/main.css">
<!--===============================================================================================-->

<style>
.footar{
					background:rgb(239, 56, 41);
					position:fixed;
					bottom:0px;
					right:0px;
					width:100%;
					height: 50px;
				    padding:1%;
				}	
				
				.espacio{
				
				margin-left:80px
				
				}
				.imagenclaro {
				width: 202px;
				height: 74px;
				}

@media only screen and (max-width: 700px) {
.limiter
{


width: 100%;
    margin: 0px auto;
    margin-top: 0px;
   
    margin-bottom: 0px;

}

.container-login100 {
    width: 100%;
    min-height: 100vh;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    padding: 10px;
    background-color: #ebebeb;
}
.p-r-85 {
    padding-right: 75px;
}

.p-l-85 {
    padding-left: 75px;
}
.wrap-login100 {
    width: 400px;
    background: #fff;
    border-radius: 10px;
    position: relative;
}
.login100-form-title {
    font-family: Raleway-Medium;
    font-size: 15px;
    color: #555555;
    line-height: 1.2;
    text-transform: uppercase;
    text-align: left;
    width: 100%;
    display: block;
}

.p-b-32 {
    padding-bottom: 32px;
}

.espacio{
				
				margin-left:0px
				
				}
				.imagenclaro {
				width: 101px;
				height: 37px;
				}

}
</style>


</head>

<body>

	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<form class="login100-form validate-form flex-sb flex-w" action="../../FUNCTIONS/CRUD/ingreso_temp.php" method="post">
					<span class="login100-form-title p-b-32">
					<img src="../../DESIGN/IMG/claro_logo.svg" class="imagenclaro"/><span class="espacio" >&nbsp;</span><img src="../../DESIGN/IMG/PMma4s4rd.png" />
					</span>

					<span class="txt1 p-b-11">
						Usuario					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Usuario Requerido">
						<input class="input100" type="text" name="dXNlcmxvZw==" >
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Contrase&ntilde;a
					</span>
					<div class="wrap-input100 validate-input m-b-12" data-validate = "Contrase&ntilde;a Requerida">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="cGFzc3dvcmRsb2c=" >
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full p-b-48">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Recordarme
							</label>
						</div>

						<div>
							<!--<a href="#" class="txt3">
								Forgot Password?
							</a>-->
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Ingresar
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
	<footer class="footar"></footer>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/bootstrap/js/popper.js"></script>
	<script src="../../DESIGN/JS/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/daterangepicker/moment.min.js"></script>
	<script src="../../DESIGN/JS/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../../DESIGN/JS/main.js"></script>
</body>
</html>