 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<?php
	require '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';
	require '../../../CONNECTION/SECURITY/conex.php';
	require '../../../CONNECTION/SECURITY/session.php';

	$nombreArchivo = 'asignar_inventarios.xml';

	$objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);

	$objPHPExcel->setActiveSheetIndex(0);

	$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

	$numRows1 = $numRows - 1;

	?>

<div>
	<?php
	echo '
	<table border=1 style="display:none">
	<tr>
	<td>ICCID</td>
	<td>ID ASESOR</td>
	<td>NOMBRE asesor</td>
	<td>ID TROPA</td>
	<td>NOMBRE TROPA</td>
	<td>ID supervisor</td>
	<td>NOMBRE SUPERVISRO</td>
	</tr>';

	for($i = 2; $i <= $numRows; $i++){

		$ICCID = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
		$ID_ASESOR = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
		$NOMBRE_ASESOR = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
		$ID_TROPA = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
		$NOMBRE_TROPA = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
		$ID_SUPERVISOR = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
		$NOMBRE_SUPERVISOR = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();

		echo '<tr>';
		echo '<td>'.$ICCID.'</td>';
		echo '<td>'.$ID_ASESOR.'</td>';
		echo '<td>'.$NOMBRE_ASESOR.'</td>';
		echo '<td>'.$ID_TROPA.'</td>';
		echo '<td>'.$NOMBRE_TROPA.'</td>';
		echo '<td>'.$ID_SUPERVISOR.'</td>';
		echo '<td>'.$NOMBRE_SUPERVISOR.'</td>';
		echo '</tr>';


		$UPDATE_asignacion=mysqli_query($conex,"UPDATE inventarios SET fk_asesor='".$ID_ASESOR."', nombre_asesor='".$NOMBRE_ASESOR."', fk_tropa='".$ID_TROPA."', nombre_tropa='".$NOMBRE_TROPA."', fk_supervisor='".$ID_SUPERVISOR."', nombre_supervisor='".$NOMBRE_SUPERVISOR."', estado='ASIGNADO ASESOR', fecha_asignacion=NOW(), estado_venta='PROCESO ASIGNADO' WHERE iccid = '".$ICCID."';");

		$insert_fijo_pospago=mysqli_query($conex,"INSERT INTO gestiones_estados_iccid (iccid, estado, estado_venta, fk_asesor, fk_user, fecha_registro) VALUES ('".$ICCID."','ASIGNADO ASESOR','PROCESO ASIGNADO','".$ID_ASESOR."', '".$id_user."', NOW()); ");
	}

	echo '</table>';

?>

<div class="alert alert-success" role="alert">
    <strong><span class="fa fa-info-circle"></span>
    </strong> Se ha actualizado correctamente la informaci&oacute;n <b><?php echo $numRows1; ?> </b>registros, Gracias por su atencion.
    <br> <a style="color:black;" href="#">¿Quieres confirmar la informacion?</a> 
    <br> <a style="color:black;" href="form_inventarios_admin.php">Volver a subir otra base masiva </a> 
    <br> <a style="color:black;" href="../ADMIN/modulo_admin.php">volver al menu inicio</a> 
</div>

</div>
