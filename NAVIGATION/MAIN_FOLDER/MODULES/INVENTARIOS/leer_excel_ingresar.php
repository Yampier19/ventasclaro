 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<?php
	require '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';
	require '../../../CONNECTION/SECURITY/conex.php';

	$nombreArchivo = 'ingresar_inventarios.xml';

	$objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);

	$objPHPExcel->setActiveSheetIndex(0);

	$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

	$numRows1 = $numRows - 1;

	?>

<div>
	<?php
	echo '
	<table border=1 style="display:none">
	<tr>
	<td>Iccid</td>
	<td>Portabilidad</td>
	<td>ID asesor</td>
	<td>Nombre Asesor</td>
	<td>ID tropa</td>
	<td>Nombre Tropa</td>
	<td>ID supervisor</td>
	<td>Nombre Supervisor</td>
	<td>Estado</td>
	<td>Numero Bloque</td>
	</tr>';

	for($i = 2; $i <= $numRows; $i++){

		$ICCID = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
		$PORTABILIDAD = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
		$NUMERO_BLOQUES = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
		$ID_ASESOR = "0";
		$NOMBRE_ASESOR = "NO ASIGNADO";
		$ID_TROPA = "0";
		$NOMBRE_TROPA = "NO ASIGNADO";
		$ID_SUPERVISOR = "0";
		$NOMBRE_SUPERVISOR = "NO ASIGNADO";
		$ESTADO = "BODEGA";
		

		echo '<tr>';
		echo '<td>'.$ICCID.'</td>';
		echo '<td>'.$PORTABILIDAD.'</td>';
		echo '<td>'.$ID_ASESOR.'</td>';
		echo '<td>'.$NOMBRE_ASESOR.'</td>';
		echo '<td>'.$ID_TROPA.'</td>';
		echo '<td>'.$NOMBRE_TROPA.'</td>';
		echo '<td>'.$ID_SUPERVISOR.'</td>';
		echo '<td>'.$NOMBRE_SUPERVISOR.'</td>';
		echo '<td>'.$ESTADO.'</td>';
		echo '<td>'.$NUMERO_BLOQUES.'</td>';
		echo '</tr>';


		$insert_fijo_pospago=mysqli_query($conex,"INSERT INTO inventarios (iccid, portabilidad, fk_asesor, nombre_asesor, fk_tropa, nombre_tropa, fk_supervisor, nombre_supervisor, estado, numero_bloque, estado_venta, fecha_registro)VALUES('$ICCID','$PORTABILIDAD','$ID_ASESOR','$NOMBRE_ASESOR','$ID_TROPA','$NOMBRE_TROPA','$ID_SUPERVISOR','$NOMBRE_SUPERVISOR','$ESTADO','$NUMERO_BLOQUES','PROCESO INVENTARIO', NOW());");

	}

	echo '</table>';

?>

<div class="alert alert-success" role="alert">
    <strong><span class="fa fa-info-circle"></span>
    </strong> Se ha subido correctamente la informaci&oacute;n <b><?php echo $numRows1; ?> </b>registros, Gracias por su atencion.
    <br> <a style="color:black;" href="#">¿Quieres confirmar la informacion?</a> 
    <br> <a style="color:black;" href="form_inventarios_admin.php">Volver a subir otra base masiva </a> 
    <br> <a style="color:black;" href="../ADMIN/modulo_admin.php">volver al menu inicio</a> 
</div>

</div>
