<?php
require ('../../../CONNECTION/SECURITY/session.php');

if ($user_name != '') {
require('../../../CONNECTION/SECURITY/conex.php');
require('../../DROPDOWN/menu_ventas.php');

$variable_u = base64_encode($id_user);
$rol_p = base64_encode($id_loginrol);
if ($rol_p=='Mg==') {
$presupuesto='$ 1.000';
}else if ($rol_p=='NA==') {
$presupuesto='$ 700';
}else if ($rol_p=='NQ==') {
$presupuesto='$ 1.000';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- meta scanner-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="author" content="Christoph Oberhofer" />
        <meta name="description" content="" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="../../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>
        <link rel="icon" type="image/png" href="../../../DESIGN/IMG/favicon.ico"/>


        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>



        <?php // estilo scanner ?>
        <link href="../../../DESIGN/CSS/scanner/styles.css" rel="stylesheet"/>

        <title>Ventas Claro</title>
        <style>
            [type="date"] {
                background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;
            }
            [type="date"]::-webkit-inner-spin-button {
                display: none;
            }
            [type="date"]::-webkit-calendar-picker-indicator {
                opacity: 0;
            }

            /* custom styles */

            label {
                display: block;
            }
            input {
                border: 1px solid #c4c4c4;
                border-radius: 5px;
                background-color: #fff;
                padding: 3px 5px;
                box-shadow: inset 0 3px 6px rgba(0,0,0,0.1);
                width: 190px;
            }
            input:invalid {
                box-shadow: 0 0 1px 0.5px red;
            }

            input:focus:invalid {
                outline: none;
            }
        </style>

        <script type="text/javascript">

            function guardar_pqr() {

                var doc_cliente = $('#doc_cliente').val();
                var fijo = $('#tipo_venta').val();
                var prepago = $('#plan_movil').val();

                $.ajax(
                        {
                            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/consultar_cliente_ajax.php',

                            data:
                                    {
                                        doc_cliente: doc_cliente,
                                        fijo: fijo,
                                        prepago: prepago
                                    },
                            type: 'post',
                            beforesend: function () {
                            },

                            success: function (data)
                            {

                                $('#hola').html(data);

                            }
                        });
            }

            $(document).ready(function () {

                $('#consultar_doc').click(function ()
                {

                    guardar_pqr();

                });
            });
        </script>

        <script type="text/javascript">

            function insert_corresponde() {

                var otro_cual = $('#otro_cual').val();

                var date_fecha = $('#date_fecha').val();

                $.ajax(
                        {
                            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/consultar_correpondiente_ajax.php',

                            data:
                                    {

                                        otro_cual: otro_cual,
                                        date_fecha: date_fecha
                                    },
                            type: 'post',
                            beforesend: function () {
                            },

                            success: function (data)
                            {

                                $('#contentcorrepondiente').html(data);


                            }
                        });
            }
            $(document).ready(function () {

                $('#btn_agregar').click(function ()
                {

                    var correspodiente = $('#correspodiente').val();
                    var otro_cual = $('#otro_cual').val();

                    if (correspodiente == "") {
                        alert("El campo correpondiente no puede ser nulo");
                    } else if (otro_cual == "") {
                        alert("El capo otro cual no puede ser nulo");
                    } else {
                        insert_corresponde();
                    }
                    $("#ocultar_cual").css('display', 'none');
                    $("#ocultar_boton_otro").css('display', 'none');

                });
            });
        </script>


        <script type="text/javascript">

            function mensaje() {

                console.log("hola desde javascript");

            }



            $(document).ready(function () {

                $('#btn_guardar').click(function ()
                {

                    setTimeout(mensaje, 5000);

                });
            });
            // la venta corresponde a:
            $(document).ready(function () {

                $('#correspodiente').change(function () {

                    var correspodiente = $('#correspodiente').val();

                    if (correspodiente == "OTRO") {

                        $("#ocultar_cual").css('display', 'block');
                        $("#ocultar_boton_otro").css('display', 'block');

                    } else if (correspodiente != "OTRO") {

                        $("#ocultar_cual").css('display', 'none');
                        $("#ocultar_boton_otro").css('display', 'none');


                    }

                });
            });

        </script>
    </head>
    <style type="text/css">
        .formulario_general{
            margin-top:2%;
            margin-bottom:0%;
            margin-left:6%;
            margin-right:5%;
            width: 90%;
            height: 90%;

        }
        .accordion{

            padding: 20px 30px 30px 30px;
        }
        *{
            margin: none;
            padding: none;

        }
        .btn_accordion{
            margin: none;
        }
        .card-body{
            overflow: auto;
            height:350px;
        }
        .btn_posicion{
            margin-left:10%;
            margin-right:10%;
        }

        @media only screen and (max-width: 700px){
            .formulario_general{
                margin-top:5%;
                margin-bottom:0%;
                margin-left:0%;
                margin-right:0%;
                width: 100%;
                height: 100%;

            }
            .accordion{

                padding: 20px 30px 30px 30px;
            }
            *{
                margin: none;
                padding: none;

            }
            .btn_accordion{
                margin: none;
            }
            .card-body{
                overflow: auto;
                height:350px;
            }
            .btn_posicion{
                margin-left:2%;
                margin-right:2%;
            }
            .fecha_ggeral{
                margin-left: 2%;
            }
            .input_fecha{
                margin-left: 2%;
            }
            .bg-red{
                background-color:rgb(239, 56, 41);
            }
        }

        .bg-red{
            background-color:rgb(239, 56, 41);
        }
        #ventas_subidas{
            color: white;
            font-family: Arial;
            font-size:14px;
            padding: .5rem 1rem;
            padding-top: 0.5rem;
            padding-right: 1rem;
            padding-bottom: 0.5rem;
            padding-left: 1rem;
            margin-left: 0.5em;
            margin-right: 0em;
        }
        #ventas_subidas:hover{
            background: #000000;
            color: white;
        }
        #ajustar_1{
            margin-right: 0px !important;
            padding-right: 0px !important;
        }
        #ajustar_2{
            margin-left: 0px !important;
            padding-left: 0px !important;
        }
        .asterisco_obligatorio{
            color: red;

        }
        #ajustar_row{
            margin-left:0px !important;
            margin-right:0px !important;
        }
        .dropdown-item:hover{
            background: #007bff;
            color:#fff;
        }
        .dropdown-item.active, .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #007bff;
}
#id_cardbody{
    max-height: 500px;
}
element.style {
    max-height: 250.297px;
    overflow: hidden;
    min-height: 162px;
    min-width: 493px;
    position: absolute;
    will-change: transform;
    top: 0px;
    left: 0px;
    transform: translate3d(0px, -443px, 0px);
}
.bootstrap-select .dropdown-menu.inner {
    max-height: 250.297px;
    position: static;
    float: none;
    border: 0;
    padding: 0;
    margin: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
}

    </style>

    <body>
        <!--<script type="text/javascript" src="../../DESIGN/JS/formulario_js/jQuery_bostrap.js"></script>-->
        <script type="text/javascript" src="../../../DESIGN/JS/formulario_js/ajax_popper_bostrap.js"></script>
        <script type="text/javascript" src="../../../DESIGN/JS/formulario_js/boostrap.js"></script>

        <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/validaciones_form_sales.js"></script>

        <div class="body-general">
            <form action="../../../FUNCTIONS/CRUD/DAOform_sales.php" method="post">
                <input  type="hidden" name="tipo_form" id="tipo_form"  value="1">
                <input  type="hidden" name="variable_u" id="variable_u" value="<?php echo $variable_u; ?>">
                <div class="formulario_general" align="left">
                    <div class="fecha_general" id="contentcorrepondiente">

                        <div class="form-group row" id="ajustar_row">
                            <div class="col-md-2">
                                <label>Fecha: <span class="asterisco_obligatorio">*</span></label>
                                <input  type="date" class="input_fecha" name="date_fecha" id="date_fecha" required="required">
                            </div>
                            <div class="col-md-4"></div>

                            <?php if ($rol_p=='NA==' || $rol_p=='MQ==') {

                            ?>
                            <div class="col-md-2">
                                <label>Correspodiente<span class="asterisco_obligatorio">*</span></label>

                                <select id="correspodiente" name="correspodiente" class="form-control" required="required">
                                    <option value="">SELECCIONAR...</option>
                                    <?php $select_correponde=mysqli_query($conex,"SELECT nombre_corresponde FROM lista_corresponde");
                                    while ($correp =(mysqli_fetch_array($select_correponde))) { ?>
                                    <option value="<?php echo $correp['nombre_corresponde']; ?>"><?php echo $correp['nombre_corresponde']; ?></option>
                                    <?php  } ?>
                                    <option>OTRO</option>
                                </select>

                            </div>

                            <div class="col-md-2" id="ocultar_cual" style="display: none;">
                                <label>Cual: <span class="asterisco_obligatorio">*</span></label>
                                <input type="text" class="form-control" name="otro_cual" id="otro_cual" pattern="[A-Z |0-9 ]{1,30}" title="Por favor ingresar valores alfanuméricos de la 'A-Z' y/o '0-9' permite espacios">
                            </div>

                            <div class="col-md-2" id="ocultar_boton_otro" style="display: none;">
                                <label>&nbsp;</label>
                                <button type="button" name="btn_agregar" id="btn_agregar" class="btn btn-danger">Agregar</button>
                            </div>

                            <?php }  ?>

                        </div>

                    </div>
                    <div>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-danger collapsed btn-lg btn-block" id="btn_accordion" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            SERVICIOS
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>Tipo de Servicio <span class="asterisco_obligatorio">*</span> </label>
                                                <select id="tipo_servicio" name="tipo_servicio" class="form-control">
                                                    <option value="">SELECCIONAR...</option>
                                                    <option value="FIJO">FIJO</option>
                                                    <option value="MOVIL">MOVIL</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="select_fijo" style="display: none;">
                                                    <label>&nbsp; <span class="asterisco_obligatorio">*</span> </label>
                                                    <select id="tipo_venta" name="tipo_venta" class="form-control">
                                                        <option value="">SELECCIONAR...</option>
                                                        <option value="FIJO BIDIRECCIONAL">FIJO BIDIRECCIONAL</option>
                                                        <option value="FIJO DTH">FIJO DTH</option>
                                                        <option value="FIJO ADICIONALES">FIJO ADICIONALES</option>
                                                    </select>
                                                </div>
                                                <div class="select_movil" style="display: none;">
                                                    <label>&nbsp; <span class="asterisco_obligatorio">*</span> </label>
                                                    <select id="plan_movil" name="plan_movil" class="form-control">
                                                        <option value="">SELECCIONAR...</option>
                                                        <option value="POSPAGO">POSPAGO</option>
                                                        <option value="PREPAGO">PREPAGO</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_servicios_fijo" style="display: none;">
                                            <div class="content_fijo_bidirecional" style="display: none;">
                                                <h5 class="subtitulo_general">FIJO BIDIRECIONAL</h5>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Televisi&oacute;n <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="fijo_television1" id="fijo_television1">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option>BASICA</option>
                                                            <option>AVANZADA</option>
                                                            <option>SUPERIOR</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Internet <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="fijo_internet1" id="fijo_internet1">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option>5 MB</option>
                                                            <option>10 MB</option>
                                                            <option>15 MB</option>
                                                            <option>25 MB</option>
                                                            <option>30 MB</option>
                                                            <option>40 MB</option>
                                                            <option>50 MB</option>
                                                            <option>60 MB</option>
                                                            <option>80 MB</option>
                                                            <option>100 MB</option>
                                                            <option>150 MB</option>
                                                            <option>200 MB</option>
                                                            <option>300 MB</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Telefon&iacute;a <span class="asterisco_obligatorio">*</span> </label>
                                                        <input  type="text" class="form-control" name="fijo_telefonia1" id="fijo_telefonia1" value="local ilimitada
                                                                " readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea class="form-control" name="fijo_observaciones1" id="fijo_observaciones1"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="content_fijo_dth" style="display: none;">
                                                <h5>FIJO DTH </h5>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Televisi&oacute;n <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="fijo_television2" id="fijo_television2">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option>BASICA</option>
                                                            <option>AVANZADA</option>
                                                            <option>SUPERIOR</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Internet <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="fijo_internet2" id="fijo_internet2">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option>35 GB</option>
                                                            <option>45 GB</option>
                                                            <option>50 GB</option>
                                                            <option>60 GB</option>
                                                            <option>100 GB</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Telefon&iacute;a <span class="asterisco_obligatorio">*</span> </label>
                                                        <input  type="text" class="form-control" name="fijo_telefonia2" id="fijo_telefonia2" value="local ilimitada" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea class="form-control" name="fijo_observaciones2" id="fijo_observaciones2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content_adicionales" style="display: none;">
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Adicionales <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="adicionales_tv" id="adicionales_tv">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option>FOX</option>
                                                            <option>HBO</option>
                                                            <option>HOT PACK</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea class="form-control" name="adici_observaciones" id="adici_observaciones"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_servicios_movil" style="display: none;">
                                            <?php ?>
                                            <div class="plan_movil_prepago" style="display: none;">
                                                <h5>M&oacute;vil</h5>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Prepago <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="movil_prepago" id="movil_prepago">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option value="PORTACION">PORTACION</option>
                                                            <option value="LINEA NUEVA (WB)">LINEA NUEVA (WB)</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="form-group row" id="div_row10" style=" display: none">
                                                    <div class="col-md-6">
                                                        <label>COSTO SIM CARD</label>
                                                        <input  type="text" class="form-control" readonly="readonly" name="movil_sim" id="movil_sim" value="<?php echo $presupuesto ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea class="form-control" name="prepago_observac" id="prepago_observac" pattern="[A-Z|0-9]{1,200}" title="Por favor ingresar valores alfanuméricos de la 'A-Z' y '0-9' "></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php ?>
                                            <?php ?>
                                            <div class="plan_movil_pospago" style="display: none;">
                                                <h5>M&oacute;vil</h5>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <label>Pospago <span class="asterisco_obligatorio">*</span> </label>
                                                        <select class="form-control" name="movil_pospago" id="movil_pospago">
                                                            <option value="">SELECCIONAR...</option>
                                                            <option value="LINEA NUEVA">LINEA NUEVA</option>
                                                            <option value="PORTACION">PORTACION</option>
                                                            <option value="MIGARCION">MIGARCION</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>PLAN <span class="asterisco_obligatorio">*</span> </label>

                                                        <select class="form-control" name="movil_plan" id="movil_plan">
                                                             <option value="">SELECCIONAR...</option>
                                                             <?php $select_planes=mysqli_query($conex,"SELECT nombre_plan,gb_plan,valor_plan,apps FROM planes_postpago");
                                    while ($plan =(mysqli_fetch_array($select_planes))) {
                                        $valor_decimales = $plan['valor_plan'];

                                        ?>

                                    <option value="<?php echo $plan['nombre_plan']." ".$plan['gb_plan']." con valor de $ ".number_format($valor_decimales,0,',','.')." con ".$plan['apps']." apps incluidas"; ?>"><?php echo $plan['nombre_plan']." ".$plan['gb_plan']." con valor de $ ".number_format($valor_decimales,0,',','.')." con ".$plan['apps']." apps incluidas"; ?></option>
                                    <?php  } ?>
                                                        </select>
                                                        <input  type="hidden" class="form-control" value="<?php echo $valor_decimales ?>" name="valor_planes" id="valor_planes" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea class="form-control" name="pospago_observac" id="pospago_observac" pattern="[A-Z|0-9]{1,200}" title="Por favor ingresar valores alfanuméricos de la 'A-Z' y '0-9' "></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-danger collapsed btn-lg btn-block" id="btn_accordion" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            INFORMACI&Oacute;N DEL CLIENTE
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>N&uacute;mero Documento <span class="asterisco_obligatorio">*</span> </label>
                                                <input type="text" class="form-control" name="doc_cliente" id="doc_cliente" required="required" pattern="[A-Z|0-9]{1,20}" title="Por favor ingresar valores alfanuméricos de la 'A-Z' y/o '0-9' ">
                                            </div>
                                            <div class="col-md-6">
                                                <label>&nbsp;</label>
                                                <input  type="button" name="consultar_doc" id="consultar_doc" value="Consultar">
                                            </div>
                                        </div>
                                        <div id="hola"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-danger collapsed btn-lg btn-block" id="btn_accordion" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            INFORMACI&Oacute;N DEL ASESOR
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>Tropa <span class="asterisco_obligatorio">*</span> </label>
                                                <select class="form-control" name="tropa_asesor" id="tropa_asesor">
                                                    <?php
                                                    if ($rol_p=='NA==' || $rol_p=='MQ==') {  //4

                                                    $select_tropa_caso_1=mysqli_query($conex,"SELECT * FROM tropa WHERE activo = '1'; ");
                                                    while ($tropa_1 =(mysqli_fetch_array($select_tropa_caso_1))) { ?>
                                                    ?>

                                                    <option value="<?php echo $tropa_1['nombre']; ?>"><?php echo $tropa_1['nombre']; ?></option>

                                                    <?php  }}else if ($rol_p=='Mg==') //2
                                                    {

                                                    $select_tropa_caso_2=mysqli_query($conex,"SELECT * FROM tropa WHERE id_user_supervisor='".$id_user."' AND activo = '1'; ");
                                                    while ($tropa_2 =(mysqli_fetch_array($select_tropa_caso_2))) { ?>
                                                    ?>

                                                    <option value="<?php echo $tropa_2['nombre']; ?>"><?php echo $tropa_2['nombre']; ?></option>

                                                    <?php }}else if ($rol_p=='NQ==') //5
                                                    {

                                                    $select_tropa_caso_3=mysqli_query($conex,"SELECT * FROM asesor AS A
                                                    LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa
                                                    WHERE fk_user='".$id_user."' AND activo = '1'; ");
                                                    while ($tropa_3 =(mysqli_fetch_array($select_tropa_caso_3))) { ?>
                                                    ?>

                                                    <option value="<?php echo $tropa_3['nombre']; ?>"><?php echo $tropa_3['nombre']; ?></option>

                                                    <?php }} ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Asesor <span class="asterisco_obligatorio">*</span> </label>
                                                <?php if ($rol_p=='NA==' || $rol_p=='MQ==') { //4 ?>
                                                <select name="id_asesorr" id="id_asesorr" class="form-control">
                                                    <option value="">SELECCIONAR...</option>
                                                    <?php
                                                    $id_usuario = $variable_u;
                                                    $id_user = base64_decode($id_usuario);

                                                    $select_asesor=mysqli_query($conex,"SELECT A.id_asesor AS id_asesor, A.nombres AS nombres, A.apellidos AS apellidos, B.nombre AS tropa FROM asesor AS A
                                                        LEFT JOIN tropa AS B ON B.id_tropa = A.fk_tropa
                                                        WHERE A.estado = '1';");
                                                    while ($asesor =(mysqli_fetch_array($select_asesor))) {
                                                    ?>
                                                    <option value="<?php echo $asesor['id_asesor']; ?>"><?php echo $asesor['nombres']." ".$asesor['apellidos']." tropa :".$asesor['tropa']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php }   ?>
                                                <?php

                                                if ($rol_p=='Mg==') { //2 ?>
                                                <select class="form-control" name="id_asesorr" id="id_asesorr">
                                                    <?php

                                                    $consultar_tropa=mysqli_query($conex,"SELECT * FROM `user` AS A
                                                    lEFT JOIN tropa AS B ON A.id_user = B.id_user_supervisor
                                                    WHERE A.id_user = '".$id_user."'; ");
                                                    while ($sub_tropa =(mysqli_fetch_array($consultar_tropa))) {

                                                    $id_tropa = $sub_tropa['id_tropa'];

                                                    }

                                                    /*$lista_asesores=mysqli_query($conex,"SELECT * FROM asesor AS A
                                                    LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa
                                                    WHERE A.fk_tropa='".$id_tropa."'; ");
                                                    while ($asesores =(mysqli_fetch_array($lista_asesores))) */

                                                    $lista_asesores=mysqli_query($conex,"SELECT A.id_asesor AS id_asesor, A.nombres AS nombres, A.apellidos AS apellidos, B.nombre AS tropa FROM asesor AS A
                                                    LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa
                                                    WHERE A.fk_tropa='2' OR A.fk_tropa='3'; ");
                                                    while ($asesores =(mysqli_fetch_array($lista_asesores)))
                                                        { ?>



                                                    <option value="<?php echo $asesores['id_asesor']; ?>"><?php echo $asesores['nombres']." ".$asesores['apellidos']." tropa: ".$asesores['tropa']; ?></option>

                                                    <?php

                                                    }
                                                    ?>
                                                </select>
                                                <?php }else if($rol_p=='NQ=='){//5  ?>

                                                <select class="form-control" name="id_asesorr" id="id_asesorr">

                                                    <?php

                                                    $select_tropa_asesor=mysqli_query($conex,"SELECT * FROM `user` AS A
                                                    LEFT JOIN asesor AS B ON A.id_user = B.fk_user
                                                    WHERE A.id_user='".$id_user."'; ");

                                                    while ($tropa_a =(mysqli_fetch_array($select_tropa_asesor))) { ?>

                                                    <option value="<?php echo $tropa_a['id_asesor']; ?>"><?php echo $tropa_a['names'].$tropa_a['surnames']; ?></option>

                                                    <?php } ?>
                                                </select>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>Supervisor(a) <span class="asterisco_obligatorio">*</span> </label>
                                                <select class="form-control" name="supervisor_asesorr" id="supervisor_asesor">
                                                    <?php if ($rol_p=='NA==' || $rol_p=='MQ==') { //4 ?>

                                                    <?php $select_supervisor_1=mysqli_query($conex,"SELECT * FROM user AS A
                                                    LEFT JOIN userlogin AS B ON A.id_user = B.id_user
                                                    WHERE B.fk_rol = '2' AND A.estado='1'; ");
                                                    while ($supervisor_1 =(mysqli_fetch_array($select_supervisor_1))) { ?>

                                                    <option value="<?php echo $supervisor_1['id_user']; ?>"><?php echo $supervisor_1['names']." ".$supervisor_1['surnames']; ?></option>

                                                    <?php  } ?>
                                                    <?php }else if ($rol_p=='Mg==') { //2?>

                                                    <?php

                                                    $select_supervisor_2=mysqli_query($conex,"SELECT * FROM user AS A
                                                    LEFT JOIN tropa AS B ON A.id_user = B.id_user_supervisor
                                                    WHERE A.id_user='".$id_user."' AND A.estado='1'; ");
                                                    while ($supervisor_2 =(mysqli_fetch_array($select_supervisor_2))) { ?>

                                                    <option value="<?php echo $supervisor_2['id_user']; ?>"><?php echo $supervisor_2['names']." ".$supervisor_2['surnames']; ?></option>

                                                    <?php  } ?>
                                                    <?php }else if($rol_p=='NQ==') { //5 ?>
                                                    <?php $select_supervisor_3=mysqli_query($conex,"SELECT * FROM asesor AS A
                                                    LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa
                                                    LEFT JOIN user AS C ON  B.id_user_supervisor = C.id_user
                                                    WHERE A.fk_user='".$id_user."' AND activo = '1'; ");
                                                    while ($supervisor_3 =(mysqli_fetch_array($select_supervisor_3))) { ?>

                                                    <option value="<?php echo $supervisor_3['id_user']; ?>"><?php echo $supervisor_3['names']." ".$supervisor_3['surnames']; ?></option>
                                                    <?php  } ?>
                                                    <?php } ?>


                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Ciudad <span class="asterisco_obligatorio">*</span> </label>
                                                <select class="form-control" id="ciudad_tropa" name="ciudad_tropa">
                                                    <?php
                                                    if ($rol_p=='NA==' || $rol_p=='MQ==') { //4 ?>

                                                    <?php
                                                    $select_ciudad_1=mysqli_query($conex,"SELECT DISTINCT(B.nombre) FROM tropa AS A
                                                    LEFT JOIN ciudad_cliente AS B ON A.fk_ciudad = B.id_ciudad_c
                                                    WHERE B.activo='1'; ");

                                                    while ($ciudad_1 =(mysqli_fetch_array($select_ciudad_1))) { ?>

                                                    <option value="<?php echo $ciudad_1['id_ciudad_c']; ?>"><?php echo $ciudad_1['nombre']; ?></option>

                                                    <?php } ?>

                                                    <?php }else if ($rol_p=='Mg==') { //2 ?>

                                                    <?php
                                                    $select_ciudad_2=mysqli_query($conex,"SELECT * FROM tropa AS A
                                                    LEFT JOIN ciudad_cliente AS B ON A.fk_ciudad = B.id_ciudad_c
                                                    WHERE A.id_user_supervisor='".$id_user."' AND B.activo='1'; ");

                                                    while ($ciudad_2 =(mysqli_fetch_array($select_ciudad_2))) { ?>

                                                    <option value="<?php echo $ciudad_2['id_ciudad_c']; ?>"><?php echo $ciudad_2['nombre']; ?></option>

                                                    <?php } ?>

                                                    <?php }else if ($rol_p=='NQ==') { //5 ?>
                                                    <option value="">Bogota</option>

                                                    <?php /*
                                                    $select_ciudad_3=mysqli_query($conex,"SELECT B.id_ciudad_c AS id_ciudad, B.nombre AS nombre_ciudad FROM tropa AS A
                                                    LEFT JOIN ciudad_cliente AS B ON A.fk_ciudad = B.id_ciudad_c
                                                    LEFT JOIN asesor AS C ON A.id_tropa = C.fk_tropa
                                                    WHERE C.id_asesor = '".$id_user."' AND B.activo='1' ");

                                                    while ($ciudad_3 =(mysqli_fetch_array($select_ciudad_3))) { ?>

                                                    <option value="<?php echo $ciudad_3['id_ciudad']; ?>"><?php echo $ciudad_3['nombre_ciudad']; ?></option>

                                                    <?php }*/ ?>

                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingfour">
                                        <h2 class="mb-0">
                                            <button class="btn btn-danger collapsed btn-lg btn-block" id="btn_accordion" type="button" data-toggle="collapse" data-target="#collapseFourt" aria-expanded="false" aria-controls="collapseFourt">
                                                DATOS DE LA VENTA
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseFourt" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
                                        <div class="card-body" id="id_cardbody">
                                            <div class="form-group row" id="div_row1" style=" display: none">
                                                <div class="col-md-6">
                                                    <!--<label>N&uacute;mero a Migrar o Portar</label>
                                                    <input  type="text" class="form-control" name="" id="">-->
                                                    <label>Cuenta <span class="asterisco_obligatorio">*</span> </label>
                                                    <input type="text" class="form-control" name="venta_cuenta" id="venta_cuenta" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row2" style="display: none">
                                                <div class="col-md-6">
                                                    <label>Ot <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="venta_ot" id="venta_ot" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row3" style=" display: none">
                                                <div class="col-md-6" id="div_min">
                                                    <label>Min <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="min" id="min" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row4" style=" display: none">
                                                <div class="col-md-6" id="div_min_temporal">
                                                    <label>Min Temporal <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="min_temporal" id="min_temporal" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row5" style=" display: none">
                                                <div class="col-md-6" id="div_operador_donante">
                                                    <label>Operador Donante <span class="asterisco_obligatorio">*</span></label>
                                                    <select class="form-control" name="venta_op_donante" id="venta_op_donante">
                                                        <option value="">SELECIONAR</option>
                                                        <option>AVANTEL</option>
                                                        <option>ETB</option>
                                                        <option>FLASH MOBILE</option>
                                                        <option>MOVIL EXITO</option>
                                                        <option>MOVISTAR</option>
                                                        <option>TIGO</option>
                                                        <option>VIRGIN MOBILE</option>
                                                        <option>OTRO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row6" style="display: none">
                                                <div class="col-md-6" id="div_definitivo">
                                                    <label>Linea aportar <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="min_definitivo" id="min_definitivo" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row7" style=" display: none">
                                                <div class="col-md-6" id="div_nnaa">
                                                    <label>Nip <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="venta_ni" id="venta_ni" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>
                                            <div class="form-group row" id="div_row8" style=" display: none">
                                                <div class="col-md-6" id="div_min_a_migrar">
                                                    <label>Min a migrar <span class="asterisco_obligatorio">*</span> </label>
                                                    <input  type="text" class="form-control" name="min_a_migrar" id="min_a_migrar" required="required" pattern="[0-9]{1,30}" title="Por favor ingresar valores numericos del '0-9' ">
                                                </div>
                                            </div>

                                            <div class="form-group row" id="div_row11" style=" display: none">

                                                <div class="col-md-6" id="div_id_generado_sistema">
                                                    <label>La venta fue subida a la plataforma: <span class="asterisco_obligatorio">*</span> </label>
                                                    <select class="form-control" name="plataforma" id="plataforma" required="required">
                                                        <option value="">SELECIONAR</option>
                                                        <option>POLIEDRO MOVIL</option>
                                                        <option>POLIEDRO CADENA</option>
                                                    </select>
                                                    <label>Id Generado por el sistema <span class="asterisco_obligatorio">*</span> </label>
                                                    <input type="text" class="form-control" name="id_generado_sistema" id="id_generado_sistema" required="required" pattern="[0-9|-]{1,30}" title="Por favor ingresar valores numericos del '0-9'">
                                                </div>


                                            </div>


                                            <div class="form-group row" id="div_row9" style=" display: none">
                                                <div class="col-md-6" id="div_tipo_iccid">
                                                    <label>Ingresar iccid por medio de:<span class="asterisco_obligatorio">*</span> </label>
                                                    <select class="form-control" name="ingreso_iccid" id="ingreso_iccid" required="required">
                                                        <option value="">SELECIONAR</option>
                                                        <option value="MANUALMENTE">Manualmente</option>
                                                        <?php //<option value="SCANNER">Scanner de codigo de barras</option> ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row" id="div_row12" style=" display: none">
                                                <!-- Button trigger modal -->
                                                <div class="col-md-6" id="div_iccid">



                                                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                        Scannear
                                                    </button>

                                                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <section id="container" class="container">

                                                                    <div class="controls">
                                                                        <fieldset class="input-group">
                                                                            <button class="stop" style="display: none;">Stop</button>
                                                                        </fieldset>
                                                                        <fieldset class="reader-config-group">
                                                                            <label style="display: none;">
                                                                                <span>Barcode-Type</span>
                                                                                <select name="decoder_readers">
                                                                                    <option value="code_128" selected="selected">Code 128</option>
                                                                                    <option value="code_39">Code 39</option>
                                                                                    <option value="code_39_vin">Code 39 VIN</option>
                                                                                    <option value="ean">EAN</option>
                                                                                    <option value="ean_extended">EAN-extended</option>
                                                                                    <option value="ean_8">EAN-8</option>
                                                                                    <option value="upc">UPC</option>
                                                                                    <option value="upc_e">UPC-E</option>
                                                                                    <option value="codabar">Codabar</option>
                                                                                    <option value="i2of5">Interleaved 2 of 5</option>
                                                                                    <option value="2of5">Standard 2 of 5</option>
                                                                                    <option value="code_93">Code 93</option>
                                                                                </select>
                                                                            </label>
                                                                            <label style="display: none;">
                                                                                <span>Resolution (width)</span>
                                                                                <select name="input-stream_constraints">
                                                                                    <option value="320x240">320px</option>
                                                                                    <option selected="selected" value="640x480">640px</option>
                                                                                    <option value="800x600">800px</option>
                                                                                    <option value="1280x720">1280px</option>
                                                                                    <option value="1600x960">1600px</option>
                                                                                    <option value="1920x1080">1920px</option>
                                                                                </select>
                                                                            </label>
                                                                            <label style="display: none;">
                                                                                <span>Patch-Size</span>
                                                                                <select name="locator_patch-size">
                                                                                    <option value="x-small">x-small</option>
                                                                                    <option value="small">small</option>
                                                                                    <option selected="selected" value="medium">medium</option>
                                                                                    <option value="large">large</option>
                                                                                    <option value="x-large">x-large</option>
                                                                                </select>
                                                                            </label>
                                                                            <label style="display: none;">
                                                                                <span>Half-Sample</span>
                                                                                <input type="checkbox" checked="checked" name="locator_half-sample" />
                                                                            </label>
                                                                            <label style="display: none;">
                                                                                <span>Workers</span>
                                                                                <select name="numOfWorkers">
                                                                                    <option value="0">0</option>
                                                                                    <option value="1">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option selected="selected" value="4">4</option>
                                                                                    <option value="8">8</option>
                                                                                </select>
                                                                            </label>
                                                                            <label>
                                                                                <span>Camera</span>
                                                                                <select name="input-stream_constraints" id="deviceSelection">
                                                                                </select>
                                                                            </label>
                                                                            <label>
                                                                                <span>Zoom</span>
                                                                                <select name="settings_zoom"></select>
                                                                            </label>
                                                                            <label>
                                                                                <span>Torch</span>
                                                                                <input type="checkbox" name="settings_torch" />
                                                                            </label>
                                                                        </fieldset>
                                                                    </div>
                                                                    <div id="result_strip">
                                                                        <ul class="thumbnails"></ul>
                                                                        <ul class="collector"></ul>
                                                                    </div>
                                                                    <div id="interactive" class="viewport"></div>
                                                                </section>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>

                                            <div class="form-group row" id="div_row13" style=" display: none">

                                                <div class="col-md-6" id="div_iccid_manual">

                                                    
                                                    <label>Iccid <span class="asterisco_obligatorio">*</span> </label>

     <!-- <iframe class="menu_principal" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/select_iccid_ajax.php" name="info" id="info"></iframe> -->

<select class="form-control selectpicker" name="venta_iccid" data-live-search="true">

    <option value="" data-tokens="">SELECCIONAR...</option>

 <?php if ($rol_p=='NQ==') { 

$slect_id_asesor = mysqli_query($conex,"SELECT id_asesor FROM asesor WHERE fk_user='".$id_user."';"); 

while ($asesor_id =(mysqli_fetch_array($slect_id_asesor))) {

                 $id_asesor = $asesor_id['id_asesor'];
                  }

$consultar_iccid_asignados=mysqli_query($conex,"SELECT iccid FROM inventarios AS A LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa  WHERE fk_asesor = '".$id_asesor."' AND estado_venta = 'PROCESO ASIGNADO' ORDER BY id_iccid DESC LIMIT 2000;
    ");
while ($ICIIDS =(mysqli_fetch_array($consultar_iccid_asignados))) {  

    $iccid = $ICIIDS['iccid'];

    ?>

                <option value="<?php echo $iccid ?>" data-tokens="<?php echo $iccid ?>"><?php echo $iccid ?></option>  

        <?php      } ?>

<?php }else if ($rol_p=='NA==' || $rol_p=='MQ==' || $rol_p=='Mg==') { 

$consultar_iccid_asignados = mysqli_query($conex,"SELECT iccid FROM inventarios AS A LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa WHERE estado_venta = 'PROCESO ASIGNADO' ORDER BY id_iccid DESC LIMIT 2000;");

while ($ICIIDS =(mysqli_fetch_array($consultar_iccid_asignados))) {

                 $iccid = $ICIIDS['iccid'];
 ?>
  <option value="<?php echo $iccid ?>" data-tokens="<?php echo $iccid ?>"><?php echo $iccid ?></option>  
                  
 <?php } } ?>

</select>

<div>

</div>
                                                    
                                              
                                                </div>

                                                <div class="col-md-6">


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="ocultar_btn" style=" display: none">
                    <center><button type="submit" class="btn btn-danger" id="btn_guardar" name="btn_guardar">Guardar</button></center>
                </div>
 
            </form>
        </div>

        <?php // scanner ?>

        <?php //<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/QUAGGAJS/serratus-quaggaJS-862df88/example/web_quaggajs.js" type="text/javascript"></script>?>
        <?php //<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/QUAGGAJS/serratus-quaggaJS-862df88/dist/quagga.js" type="text/javascript"></script> ?>
        <?php //<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/QUAGGAJS/serratus-quaggaJS-862df88/example/live_w_locator.js" type="text/javascript"></script>?>
    </body>
</html>

<?php
}else {
echo 'Usted no tiene los permisos necesarios para acceder a esta informacion' . $user_name;
}

?>
