<?php
require ('../../../CONNECTION/SECURITY/session.php');

if ($user_name != '') {

require('../../../CONNECTION/SECURITY/conex.php');
require('../../DROPDOWN/menu_ventas.php'); 
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ventasclaro</title>
	 <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- meta scanner-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="author" content="Christoph Oberhofer" />
        <meta name="description" content="" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="../../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>
        <link rel="icon" type="image/png" href="../../../DESIGN/IMG/favicon.ico"/>

        <link href="../../../DESIGN/CSS/jquery.dataTables.min.css" rel="stylesheet"/>
		<script src="../../../DESIGN/JS/data_tables/jquery.dataTables.js" rel="stylesheet"></script>
</head>
<style type="text/css">

	.contenedor_de_tabla{
		background: ;
		margin-top: 2%;
		margin-left: 1%;
		margin-bottom: 2%;
		margin-right: 1%;
		padding: 0.5%;
		border-style: double;
		border-color: black;
		box-shadow: 0px 0px 15px #666; 
	}
    .espacio{
    	height:43px;
    	overflow: auto;
    }
    td{
    	padding-top: 0px !important;
    	padding-bottom:0px !important; 
    	padding-left:0.5% !important;
    	padding-right:0.5% !important;
    }
   th{
   	padding: 0.5% !important;
   	
   }
	.encabezado_Tabla1{
		background: rgb(239, 56, 41);
		color: white;
		font-family: Arial Narrow;
		font-size: 16px;
	}
	#usertable{
		border: 1.5px #666666;

	}
	#letra_a{
		font-family: Arial Narrow;
		font-size: 14px;
	}
	#ajustar_tabla{
		
		/*width: 80% !important;
		height: 80% !important;*/

	}

</style>
	<script type="text/javascript">
            $(document).ready(function () {
  
                $('#usertable').DataTable();
            });
        </script>

        <script type="text/javascript">
        	function ventanaSecundaria (URL){   window.open(URL,"ventana1","width=1300,height=500,Top=150,Left=50%") }
        </script>
<body>
	 <script type="text/javascript" src="../../../DESIGN/JS/formulario_js/ajax_popper_bostrap.js"></script>
        <script type="text/javascript" src="../../../DESIGN/JS/formulario_js/boostrap.js"></script>

       <?php if ($id_loginrol=='2') { ?>
       	<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">

    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>ESTADO ICCID</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
					<th id=""><center>INSIDENCIAS</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa  WHERE estado != 'BODEGA' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {

				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 
?>
	 <tr role='row'>
				 <td><span class='descripcion'><?php echo $iccid; ?></span></td>
				 <td><span class='descripcion'><?php echo $portabilidad; ?></span></td>
				 <td><span class='descripcion'><?php echo $nombre_asesor; ?></span></td>
				 <td><span class='descripcion'><p class='espacio'><?php echo $nombre_tropa; ?></p></span></td>
				 <td><span class='descripcion'><?php echo $nombre_supervisor; ?></span></td>
				 <td><span class='descripcion'><?php echo $estado; ?></span></td>
				 <td><span class='descripcion'><?php echo $fecha_asignacion; ?></span></td>
				 <td><span class='descripcion'><?php echo $estado_venta; ?></span></td>
				 <td>
				 	<span class='descripcion'></span>
				 	<input class="btn btn-outline-dark" type="button" name="insidencias" id="insidencias" title="INSIDENCIAS" style="width:100%; height:50px" value="insidencias" onclick="javascript:ventanaSecundaria('../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/ventana_emegente_insidencias.php?xdfghx=<?php echo base64_encode($iccid) ?>')"/>
				</td>
		</tr>	
			<?php  }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>

<?php }else if($id_loginrol=='5'){ ?>

<?php 

$slect_id_asesor = mysqli_query($conex,"SELECT id_asesor FROM asesor WHERE fk_user='".$id_user."';"); 

while ($asesor_id =(mysqli_fetch_array($slect_id_asesor))) {

				 $id_asesor = $asesor_id['id_asesor'];
				  }
?>
<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">

    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>ESTADO ICCID</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa  WHERE fk_asesor = '".$id_asesor."' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {

				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 $fecha_registro = $inventario['fecha_registro'];

	 echo "<tr role='row'>
				 <td><span class='descripcion'>".$iccid."</span></td>
				 <td><span class='descripcion'>".$portabilidad."</span></td>
				 <td><span class='descripcion'>".$nombre_asesor."</span></td>
				 <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
				 <td><span class='descripcion'>".$nombre_supervisor."</span></td>
				 <td><span class='descripcion'>".$estado."</span></td>
				 <td><span class='descripcion'>".$fecha_asignacion."</span></td>
				 <td><span class='descripcion'>".$estado_venta."</span></td>
				
		</tr>";	
			 }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>

<?php  } ?>

<?php if (isset($_GET['hthsddf'])) {  ?>

	<h5>jala</h5>
	
<?php  } ?>


</body>
</html>
<?php }else {
echo 'Usted no tiene los permisos necesarios para acceder a esta informacion' . $user_name;
}
 ?>