
<?php 	
require ('../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
require('../../CONNECTION/SECURITY/conex.php');

include('../DROPDOWN/menu_inventarios.php');
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>imventarios</title>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
	<link href="../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>
	<link href="../../DESIGN/CSS/data_tables/jquery.dataTables.css" rel="stylesheet"/>
	<script src="../../DESIGN/JS/data_tables/jquery.dataTables.js" rel="stylesheet"></script> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  </head>
  <style type="text/css">
	a {
    color: #ffffff;
    text-decoration: none;
    background-color: transparent;
    }

    .posicion{
    	margin-top: 15%;
    }
    .card-header{
    	background: #ef3829;
    	color: white;
    }
    #agrupacion_n1{
    	border:1px solid #ccc;
    	margin-top:1%;
    	margin-bottom: 1%;
    	padding: 1%;	
    }
    #agrupacion_n2{
    	border:1px solid #ccc;
    	margin-top:1%;
    	margin-bottom: 1%;
    	padding: 1%;
    }
    #descargar_plan{
    	
    }
    #subir_inventario{
    		margin-top: 0.4%;	
    }
    #nombre_archivo{
    	padding: 7.5px;
    	width: 100%;
    }
    #nombre_archivo:hover{
    		background: #dae0e5;

}
</style>
  <body>	
  	<div class="posicion" align="center">	
<div class="card border-dark mb-3" style="max-width: 51.5rem;">
  <div class="card-header"><h4>Inventarios</h4></div>
  <div class="card-body text-dark">
    <h5 class="card-title">descargar plantilla de inventario / Subir Inventario</h5>
    <div class="alert alert-info" role="alert">
 <p class="card-text"><strong><span class="fa fa-info-circle"></span></strong> Instruciones: Por favor descargar el plantilla de inventarios.
     Llenar la informacion de inventarios <b>sin cambiar el plantilla y el encabezado</b>, luego adjuntar el archivo <b>sin cambiar el nombre ni la extencion del archivo,</b> Gracias por su atencion.</p>
</div>
    <div class="form-group row">
     <div class="col-md-6" id="agrupacion_n1" name="agrupacion_n1">
     	<label id="nombre_archivo">inventarios.xls</label>
     	<form action="../EXPORTABLES/plantilla_inventarios.php" method="post">
     	<button class="btn btn-outline-danger btn-lg btn-block" formaction="../EXPORTABLES/plantilla_inventarios.php" name="descargar_plan" id="descargar_plan" type="submit"><span class="fa fa-cloud-download"></span>&nbsp;Descargar Plantilla</button>
     	</form>
     </div>
     <div class="col-md-6" id="agrupacion_n2" name="agrupacion_n2">
     	<form id="FormData"  name="FormData" content="text/html;" enctype="multipart/form-data" method="post">
			<input type="file" class="btn btn-light" name="id_excel" id="id_excel"><br>	
			<button class="btn btn-outline-danger btn-lg btn-block" name="subir_inventario" id="subir_inventario" type="submit"><span class="fa fa-cloud-upload"></span>&nbsp;Subir Inventario</button>
		</form>	
     </div>
    </div>
    <div class="form-group row">
     <div class="col-md-12">	
     	<?php 
$tamano = $_FILES["id_excel"]['size'];
$tipo = $_FILES["id_excel"]['type'];
$archivo1 = $_FILES["id_excel"]['name'];
$error = $_FILES['id_excel']['error'];
$archivo1 = str_replace(" ","_",$archivo1);
   $prefijo1 = $archivo1;

    if ($archivo1 != "") {
       // guardamos el archivo a la carpeta files
       $destino =  "../EXPORTABLES/".$archivo1;
       
if (copy($_FILES['id_excel']['tmp_name'],$destino)) {
           $status = "Archivo subido: <b>".$archivo1."</b> fue subido con exito";
           echo "<div class='alert alert-success' role='alert'>
    	 	<p class='card-text'>";
           echo $status;
           echo "</p>
</div>";
echo "<html><script> alert ('Se ha subido correctamente el archivo por seguidad recargaremos la pagina'); window.location.href = 'form_subir_inventarios.php';</script> </html>";
       } else {
       	echo "<div class='alert alert-danger' role='alert'>
    	 	<p class='card-text'>";
           $status = "Error al subir archivo";
           echo $status;    
                       echo "</p>
</div>";  

}
  }
 ?>
     </div>
     </div>
  </div>
</div>
  </body>
</div>

<?php 
}else { ?>

<?php echo 'Usted no tiene los permisos necesarios para acceder a esta informacion ' . $user_name;  ?>

 <?php }

?>