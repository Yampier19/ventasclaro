<?php 
require ('../../CONNECTION/SECURITY/session.php');

/*if ($user_name != '') {*/

require('../../CONNECTION/SECURITY/conex.php');

require('../DROPDOWN/index.php');

$variable_u = base64_encode($id_user);

?>
<!DOCTYPE html>
<html>
<head>
	 <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!-- JQUERY PAGINIADO-->
       
        <!-- EXPORTABLE-->
        <link href="css/style_table.css" rel="stylesheet" type="text/css">
        <link href="css/Estilo_Menu2.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>


        
        <!--ESTILO DEL PAGINIADO-->
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
     
     <?php //<link href="../../DESIGN/CSS/data_tables/jquery.dataTables.css" rel="stylesheet"/>  ?>

     <?php //<link href="../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>  ?>
        <!-- Bootstrap CSS -->
    <?php //<link href="../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>  ?>
    <link rel="icon" type="image/png" href="../../DESIGN/IMG/favicon.ico"/>
    <title>Ventas Claro</title>

    <script type="text/javascript">
            $(document).ready(function () {

                $('#usertable').DataTable();
            });
        </script>
        <script type="text/javascript">
           function ventanaSecundaria(URL)
            {
                window.open(URL, "ventana1", "width=800,height=500,Top=150,Left=50%")
            }
            function noback() {
                window.location.hash = "no-back-button";
                window.location.hash = "Again-No-back-button";
            }
        </script>
</head>
<style type="text/css">
        .formulario_general{
            margin-top:5%;
            margin-bottom:5%;
            margin-left:10%;
            margin-right:5%;
            width: 90%;
            height: 90%;

        }
        .accordion{

            padding: 20px 30px 30px 30px;
        }
        *{
            margin: none;
            padding: none;

        }
        .btn_accordion{
            margin: none;
        }
        .card-body{
            overflow: auto; 
            height:350px;
        }
        .btn_posicion{
            margin-left:10%;
            margin-right:10%;
        }
        @media only screen and (max-width: 700px){
            .formulario_general{
                margin-top:5%;
                margin-bottom:0%;
                margin-left:0%;
                margin-right:0%;
                width: 100%;
                height: 100%;

            }
            .accordion{

                padding: 20px 30px 30px 30px;
            }
            *{
                margin: none;
                padding: none;

            }
            .btn_accordion{
                margin: none;
            }
            .card-body{
                overflow: auto; 
                height:350px;
            }
            .btn_posicion{
                margin-left:2%;
                margin-right:2%;
            }
            .fecha_ggeral{
                margin-left: 2%;
            }
            .input_fecha{
                margin-left: 2%;
            }
            .bg-red{
                background-color:rgb(239, 56, 41);
            }
        }
        .bg-red{
            background-color:rgb(239, 56, 41);
        }
    </style>
    <style type="text/css">
    	#conten_general{
    		margin-top:5%; 
    		margin-bottom:0%;
    		margin-left:0%;
    		margin-right:0%;
    		height: 700px;
    	}
    </style>
<body>
	<?php //<script type="text/javascript" src="../../DESIGN/JS/formulario_js/jQuery_bostrap.js"></script> ?>
    <?php //<script type="text/javascript" src="../../DESIGN/JS/formulario_js/ajax_popper_bostrap.js"></script> ?>
    <?php //<script type="text/javascript" src="../../DESIGN/JS/formulario_js/boostrap.js"></script> ?>



        <nav class="navbar navbar-light bg-red ">
            <a class="navbar-brand" href="#">
                <img src="../../DESIGN/IMG/logo-claro-blanco.svg" class="d-inline-block align-top" width="100px"/>
            </a>
            <a class="navbar-brand" href="#"> <img src="../../DESIGN/IMG/LogoBN 100x45.png" class="d-inline-block align-top" width="100px"/></a>

            <a class="navbar-nav flex-row ml-md-auto d-md-flex" href="../../CONNECTION/SECURITY/cerrar_sesion.php"><img src="../../DESIGN/IMG/apagar-icono.png" width="24" height="24"/></a>
        </nav>
       
        	<div class="card text-center" id="conten_general">
 				<div class="card-header">
 					<h3>Registros</h3>
 				</div>
 				<div class="card-body">
 					<div class="col-sm-12" style="height: 100%">
                  <div class="tabla1">
                	<table id="usertable" class="display">
                		<thead>
                			<tr>
                				<!--cliente-->
							<td>nombre_completo</td>
							<td>tipo_documento</td>
							<td>numero_documento</td>
							<td>fecha_expedicion</td>
							<td>direccion</td>
							<td>numero_contacto</td>
							<td>correo</td>
							<td>tropa</td>
							<td>supervisor</td>
							<td>doc_supervisor</td>
							<td>fecha_venta</td>
							<!--prepago-->
							<td>prepago</td>
							<td>recargar</td>
							<td>costo_sim</td>
							<td>observaciones</td>
							<td>fecha_registro</td>
							<!--pospago-->
							<td>fecha_venta</td>
							<td>pospago</td>
							<td>plan</td>
							<td>observaciones</td>
							<td>fecha_registro</td>
							<!--triple play-->
							<td>oferta_fijo</td>
							<td>fecha_venta</td>
							<td>televiosion</td>
							<td>internet</td>
							<td>telefonia</td>
							<td>observaciones</td>
							<td>fecha_registro</td>
							<!--adicionales-->
							<td>fecha_venta</td>
							<td>canales_adicionales</td>
							<td>observaciones</td>
							<td>fecha_registro</td>
							<!--datos_venta-->
							<td>iccid</td>
							<td>nip</td>
							<td>operador_donante</td>
							<td>cuenta</td>
							<td>ot</td>
							<td>min_c</td>
							<td>min_temporal</td>
							<td>min_definitivo</td>
							<td>min_a_emigrar</td>
							<!--asesor-->
							<td>nombres</td>
							<td>apellidos</td>
							<td>numero_documento</td>
							<!--ciudad-->
							<td>ciudad</td>
                			</tr>
                		</thead>
                		
                		<tbody>
                			<?php  $select_info_general = mysqli_query($conex,"SELECT * FROM cliente AS A
								LEFT JOIN prepago AS B ON A.fk_prepago = B.id_prepago
								LEFT JOIN pospago AS C ON A.fk_pospago = C.id_pospago
								LEFT JOIN triple_play AS D ON A.fk_triple_play = D.id_triple_play
								LEFT JOIN adicionales AS E ON A.fk_adicionales = E.id_adicionales
								LEFT JOIN datos_venta AS F ON A.fk_datos_venta = F.id_datos_venta
								LEFT JOIN asesor AS G ON A.fk_asesor = G.id_asesor
								LEFT JOIN ciudad AS H ON A.fk_ciudad = H.id_ciudad
								WHERE A.fk_user = '2';"); 
								while ($datos =(mysqli_fetch_array($select_info_general))) { ?>  

                			<tr>
                				<td><?php echo $datos['nombre_completo']; ?></td>
								<td><?php echo $datos['tipo_documento']; ?></td>
								<td><?php echo $datos['numero_documento']; ?></td>
								<td><?php echo $datos['fecha_expedicion ']; ?></td>
								<td><?php echo $datos['direccion ']; ?></td>
								<td><?php echo $datos['numero_contacto']; ?></td>
								<td><?php echo $datos['correo']; ?></td>
								<td><?php echo $datos['tropa']; ?></td>
								<td><?php echo $datos['supervisor ']; ?></td>
								<td><?php echo $datos['doc_supervisor']; ?></td>
								<td><?php echo $datos['fecha_venta']; ?></td>
								<!--prepago-->
								<td><?php echo $datos['prepago']; ?></td>
								<td><?php echo $datos['recargar']; ?></td>
								<td><?php echo $datos['costo_sim']; ?></td>
								<td><?php echo $datos['observaciones']; ?></td>
								<td><?php echo $datos['fecha_registro']; ?></td>
								<!--//pospago-->
								<td><?php echo $datos['fecha_venta']; ?></td>
								<td><?php echo $datos['pospago']; ?></td>
								<td><?php echo $datos['plan']; ?></td>
								<td><?php echo $datos['observaciones']; ?></td>
								<td><?php echo $datos['fecha_registro']; ?></td>
								<!--//triple play-->
								<td><?php echo $datos['oferta_fijo']; ?></td>
								<td><?php echo $datos['fecha_venta']; ?></td>
								<td><?php echo $datos['televiosion ']; ?></td>
								<td><?php echo $datos['internet']; ?></td>
								<td><?php echo $datos['telefonia']; ?></td>
								<td><?php echo $datos['observaciones']; ?></td>
								<td><?php echo $datos['fecha_registro']; ?></td>
								<!--//adicionales-->
								<td><?php echo $datos['fecha_venta']; ?></td>
								<td><?php echo $datos['canales_adicionales']; ?></td>
								<td><?php echo $datos['observaciones']; ?></td>
								<td><?php echo $datos['fecha_registro']; ?></td>
								<!--//datos_venta-->
								<td><?php echo $datos['iccid']; ?></td>
								<td><?php echo $datos['nip']; ?></td>
								<td><?php echo $datos['operador_donante']; ?></td>
								<td><?php echo $datos['cuenta']; ?></td>
								<td><?php echo $datos['ot']; ?></td>
								<td><?php echo $datos['min_c']; ?></td>
								<td><?php echo $datos['min_temporal']; ?></td>
								<td><?php echo $datos['min_definitivo']; ?></td>
								<td><?php echo $datos['min_a_emigrar']; ?></td>
								<!--//asesor-->
								<td><?php echo $datos['nombres']; ?></td>
								<td><?php echo $datos['apellidos']; ?></td>
								<td><?php echo $datos['numero_documento']; ?></td>
								<!--//ciudad-->
								<td><?php echo $datos['ciudad']; ?></td>
                			</tr>
                			<?php ;} ?>
                		</tbody>
                		
                	</table>
                	</div>
                	</div>
 				</div>
 				<div class="card-footer text-muted" id="footer">
  				</div>
			</div>  
</body>
</html>
<script src="jquery-1.12.4.min.js"></script>
    <!-- Llamar a los complementos javascript EXPORTABLE-->
    <script src="FileSaver.min.js"></script>
    <script src="Blob.min.js"></script>
    <script src="xls.core.min.js"></script>
    <script src="dist/js/tableexport.js"></script>
    <!-- JS DE PAGINIADO-->
    <script type="text/javascript" src="js/jquery.dataTables.js"></script>
<?php  //<script type="text/javascript" src="../../DESIGN/JS/data_tables/data_tables.js"></script>?>
<?php 
/*}else {
echo 'Usted no tiene los permisos necesarios para acceder a esta informacion' . $user_name;
}*/

?>