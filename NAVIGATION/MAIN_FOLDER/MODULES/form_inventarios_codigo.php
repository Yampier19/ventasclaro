<!DOCTYPE html>
<html>
<head>
	<title></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
	<link href="../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>

	<script>
			
    		$(function(){
				// Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
				$("#adicional").on('click', function(){
					for (var i = 0; i < 1; i++) {
						$("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
					}
					
				});
			 
				// Evento que selecciona la fila y la elimina 
				$(document).on("click",".eliminar",function(){
					var parent = $(this).parents().get(0);
					$(parent).remove();
				});
			});
		</script>
		<form action="logica_codigo.php" method="post">
				<h3 class="bg-primary text-center pad-basic no-btm">Agregar Nuevo Alumno </h3>
				<table class="table bg-info"  id="tabla">
					<tr class="fila-fija">
						<td><input required name="idalumno[]" value="" placeholder="ID Alumno"/></td>
						<td><input required name="nombre[]" value="" placeholder="Nombre Alumno"/></td>
						<td><input required name="carrera[]" value="" placeholder="Carrera"/></td>
						<td><input required name="grupo[]" value="" placeholder="Grupo"/></td>
						<td class="eliminar"><input type="button"   value="Menos -"/></td>
					</tr>
				</table>

				<div class="btn-der">
					<input type="submit" name="insertar" value="Insertar Alumno" class="btn btn-info"/>
					<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Más + </button>
					<input type="submit" name="guardar" id="guardar">
				</div>
			</form>
			<span id="hola"></span>
</head>
<body>

</body>
</html>