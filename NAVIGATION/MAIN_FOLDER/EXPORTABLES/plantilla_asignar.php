<?php

	require '../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel.php';
	require '../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()
	->setCreator('Codigos de Programacion')
	->setTitle('Excel en PHP')
	->setDescription('Documento de prueba')
	->setKeywords('excel phpexcel php')
	->setCategory('Ejemplos');

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Hoja1');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ICCID');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ID ASESOR');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRES ASESOR');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ID TROPA');
	$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NOMBRE TROPA');
	$objPHPExcel->getActiveSheet()->setCellValue('F1', 'ID SUPERVISOR');
	$objPHPExcel->getActiveSheet()->setCellValue('G1', 'NOMBRE SUPERVISOR');


	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="asignar_inventarios.xml"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');


?>
