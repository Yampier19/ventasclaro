<?php

	require '../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel.php';
	require '../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()
	->setCreator('Codigos de Programacion')
	->setTitle('Excel en PHP')
	->setDescription('Documento de prueba')
	->setKeywords('excel phpexcel php')
	->setCategory('Ejemplos');

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Hoja1');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ICCID');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', 'PORTABILIDAD');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NUMERO BLOQUE');

	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="ingresar_inventarios.xml"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');


?>
