<?php 
require('../../../CONNECTION/SECURITY/conex.php');
require ('../../../CONNECTION/SECURITY/session.php');
 ?>
<!DOCTYPE html>
<html>
<head>
		<title>Ventas Claro</title> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href="../../../DESIGN/CSS/jquery.dataTables.min.css" rel="stylesheet"/>
	<script src="../../../DESIGN/JS/data_tables/jquery.dataTables.js" rel="stylesheet"></script>
 	<link href="../../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>
</head>
<body>

	<script type="text/javascript">
            $(document).ready(function () {
  
                $('#usertable').DataTable();
            });
        </script>
<style type="text/css">

	.contenedor_de_tabla{
		background: ;
		margin-top: 2%;
		margin-left: 1%;
		margin-bottom: 2%;
		margin-right: 1%;
		padding: 0.5%;
		border-style: double;
		border-color: black;
		box-shadow: 0px 0px 15px #666; 
	}
    .espacio{
    	height:43px;
    	overflow: auto;
    }
    td{
    	padding-top: 0px !important;
    	padding-bottom:0px !important; 
    	padding-left:0.5% !important;
    	padding-right:0.5% !important;
    }
   th{
   	padding: 0.5% !important;
   	
   }
	.encabezado_Tabla1{
		background: rgb(239, 56, 41);
		color: white;
		font-family: Arial Narrow;
		font-size: 16px;
	}
	#usertable{
		border: 1.5px #666666;

	}
	#letra_a{
		font-family: Arial Narrow;
		font-size: 14px;
	}

</style>

<?php 

$tropa_asesor = $_POST['tropa_asesor']; //echo $tropa_asesor ;
$id_asesorr = $_POST['id_asesorr']; //echo $id_asesorr ;
$number_block = $_POST['number_block']; //echo $number_block ;
$fechas_asignacion = $_POST['fechas_asignacion']; //echo $fechas_asignacion ;


if ($tropa_asesor != "") {

 	$tipo_consulta = "1"; // consulta 1 
 	// echo $tipo_consulta; 

 } else if ($id_asesorr != "") {

 	$tipo_consulta = "2";  // consulta 2
 	// echo $tipo_consulta;

 } else if ($number_block != "") {

 	$tipo_consulta = "3";  // consulta 3
 	// echo $tipo_consulta;

 } else if ($fechas_asignacion != "") {

 	$tipo_consulta = "4";  // consulta 4
 	// echo $tipo_consulta;
 }


 ?>

<?php if ($id_loginrol=='1' && $tipo_consulta=="1") {  ?>

	<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">

    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>ID ASESOR</center></th> 
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>NUMERO BLOQUE</center></th>
					<th id=""><center>ESTADO</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
					<th id=""><center>FECHA REGISTRO</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN tropa AS B ON A.fk_tropa = B.id_tropa WHERE B.id_tropa = '".$tropa_asesor."' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {

				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				 $fk_asesor = $inventario['fk_asesor']; 
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				 $numero_bloque = $inventario['numero_bloque'];
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 $fecha_registro = $inventario['fecha_registro'];

	 echo "<tr role='row'>
				 <td><span class='descripcion'>".$iccid."</span></td>
				 <td><span class='descripcion'>".$portabilidad."</span></td>
				 <td><span class='descripcion'>".$fk_asesor."</span></td>
				 <td><span class='descripcion'>".$nombre_asesor."</span></td>
				 <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
				 <td><span class='descripcion'>".$nombre_supervisor."</span></td>
				 <td><span class='descripcion'>".$numero_bloque."</span></td>
				 <td><span class='descripcion'>".$estado."</span></td>
				 <td><span class='descripcion'>".$fecha_asignacion."</span></td>
				 <td><span class='descripcion'>".$estado_venta."</span></td>
				 <td><span class='descripcion'>".$fecha_registro."</span></td>
		</tr>";	
			 }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>

<?php  } else if ($id_loginrol=='1' && $tipo_consulta=="2") { ?>
	<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">
    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>ID ASESOR</center></th> 
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>NUMERO BLOQUE</center></th>
					<th id=""><center>ESTADO</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
					<th id=""><center>FECHA REGISTRO</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, A.estado AS estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN asesor AS B ON A.fk_asesor = B.id_asesor WHERE B.id_asesor = '2' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {
				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				 $fk_asesor = $inventario['fk_asesor']; 
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				 $numero_bloque = $inventario['numero_bloque'];
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 $fecha_registro = $inventario['fecha_registro'];

	 echo "<tr role='row'>
				 <td><span class='descripcion'>".$iccid."</span></td>
				 <td><span class='descripcion'>".$portabilidad."</span></td>
				 <td><span class='descripcion'>".$fk_asesor."</span></td>
				 <td><span class='descripcion'>".$nombre_asesor."</span></td>
				 <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
				 <td><span class='descripcion'>".$nombre_supervisor."</span></td>
				 <td><span class='descripcion'>".$numero_bloque."</span></td>
				 <td><span class='descripcion'>".$estado."</span></td>
				 <td><span class='descripcion'>".$fecha_asignacion."</span></td>
				 <td><span class='descripcion'>".$estado_venta."</span></td>
				 <td><span class='descripcion'>".$fecha_registro."</span></td>
		</tr>";	
			 }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>
<?php  } else if ($id_loginrol=='1' && $tipo_consulta=="3") { ?>
	<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">
    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>ID ASESOR</center></th> 
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>NUMERO BLOQUE</center></th>
					<th id=""><center>ESTADO</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
					<th id=""><center>FECHA REGISTRO</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, A.estado AS estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN asesor AS B ON A.fk_asesor = B.id_asesor  WHERE numero_bloque = '".$number_block."' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {
				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				 $fk_asesor = $inventario['fk_asesor']; 
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				 $numero_bloque = $inventario['numero_bloque'];
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 $fecha_registro = $inventario['fecha_registro'];

	 echo "<tr role='row'>
				 <td><span class='descripcion'>".$iccid."</span></td>
				 <td><span class='descripcion'>".$portabilidad."</span></td>
				 <td><span class='descripcion'>".$fk_asesor."</span></td>
				 <td><span class='descripcion'>".$nombre_asesor."</span></td>
				 <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
				 <td><span class='descripcion'>".$nombre_supervisor."</span></td>
				 <td><span class='descripcion'>".$numero_bloque."</span></td>
				 <td><span class='descripcion'>".$estado."</span></td>
				 <td><span class='descripcion'>".$fecha_asignacion."</span></td>
				 <td><span class='descripcion'>".$estado_venta."</span></td>
				 <td><span class='descripcion'>".$fecha_registro."</span></td>
		</tr>";	
			 }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>
<?php  } else if ($id_loginrol=='1' && $tipo_consulta=="4") { ?>
	<div class="col-md-12">
        	<div class="contenedor_de_tabla">
                <div class="table-responsive">
    <table id="usertable" class="table table-striped">
			<thead class="encabezado_Tabla1">
				<tr>
					<th id=""><center>ICCID</center></th>
					<th id=""><center>PORTABILIDAD</center></th>
					<th id=""><center>ID ASESOR</center></th> 
					<th id=""><center>NOMBRE ASESOR</center></th>
					<th id=""><center>NOMBRE TROPA</center></th>
					<th id=""><center>NOMBRE SUPERVISOR</center></th>
					<th id=""><center>NUMERO BLOQUE</center></th>
					<th id=""><center>ESTADO</center></th>
					<th id=""><center>FECHA ASIGNACION</center></th>
					<th id=""><center>ESTADO VENTA</center></th>
					<th id=""><center>FECHA REGISTRO</center></th>
				</tr>
			</thead>
			<tbody id="letra_a">
	<?php 	
	$select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, A.estado AS estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios AS A LEFT JOIN asesor AS B ON A.fk_asesor = B.id_asesor  WHERE fecha_asignacion = '".$fechas_asignacion."' ORDER BY id_iccid DESC LIMIT 2000; "); 

		while ($inventario =(mysqli_fetch_array($select_inventarios))) {
				 $iccid = $inventario['iccid'];
				 $portabilidad = $inventario['portabilidad'];
				 $fk_asesor = $inventario['fk_asesor']; 
				 $nombre_asesor = $inventario['nombre_asesor'];
				 $nombre_tropa = $inventario['nombre_tropa'];
				 $nombre_supervisor = $inventario['nombre_supervisor'];
				 $numero_bloque = $inventario['numero_bloque'];
				 $estado = $inventario['estado'];
				 $fecha_asignacion = $inventario['fecha_asignacion'];
				 $estado_venta = $inventario['estado_venta'];
				 $fecha_registro = $inventario['fecha_registro'];

	 echo "<tr role='row'>
				 <td><span class='descripcion'>".$iccid."</span></td>
				 <td><span class='descripcion'>".$portabilidad."</span></td>
				 <td><span class='descripcion'>".$fk_asesor."</span></td>
				 <td><span class='descripcion'>".$nombre_asesor."</span></td>
				 <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
				 <td><span class='descripcion'>".$nombre_supervisor."</span></td>
				 <td><span class='descripcion'>".$numero_bloque."</span></td>
				 <td><span class='descripcion'>".$estado."</span></td>
				 <td><span class='descripcion'>".$fecha_asignacion."</span></td>
				 <td><span class='descripcion'>".$estado_venta."</span></td>
				 <td><span class='descripcion'>".$fecha_registro."</span></td>
		</tr>";	
			 }
	  ?>
	</tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>
<?php  } ?>

</body>
</html>