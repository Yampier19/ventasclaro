// JavaScript Document


            $(document).ready(function () {

                $('#tipo_servicio').change(function () {

                    var servicio = $('#tipo_servicio').val();

                    if (servicio == "FIJO") {

                        $(".select_fijo").css('display', 'block');

                        $(".select_movil").css('display', 'none');

                        $(".content_servicios_fijo").css('display', 'block');

                        $(".content_servicios_movil").css('display', 'none');

                        //datos venta

                        $("#div_row1").css('display', 'block');

                        $("#div_row2").css('display', 'block');

                        $("#ocultar_btn").css('display', 'block');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'none');

                        $("#div_row11").css('display', 'none'); 

                    } else if (servicio == "MOVIL") {

                        $(".select_fijo").css('display', 'none');

                        $(".select_movil").css('display', 'block');

                        $(".content_servicios_fijo").css('display', 'none');

                        $(".content_servicios_movil").css('display', 'block');

                    } else if (servicio == "") {

                        $(".select_fijo").css('display', 'none');

                        $(".select_movil").css('display', 'none');

                        $(".content_servicios_fijo").css('display', 'none');

                        $(".content_servicios_movil").css('display', 'none');

                        // datos venta

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'none');

                        $("#div_row11").css('display', 'none');

                        $("#ocultar_btn").css('display', 'none');

                    }
                });
                $('#tipo_venta').change(function () {

                    var ventas = $('#tipo_venta').val();

                    if (ventas == "") {

                        $(".content_fijo_bidirecional").css('display', 'none');

                        $(".content_fijo_dth").css('display', 'none');

                        $(".content_adicionales").css('display', 'none');

                    } else if (ventas == "FIJO BIDIRECCIONAL") {

                        $(".content_fijo_bidirecional").css('display', 'block');

                        $(".content_fijo_dth").css('display', 'none');

                        $(".content_adicionales").css('display', 'none');

                    } else if (ventas == "FIJO DTH") {

                        $(".content_fijo_bidirecional").css('display', 'none');

                        $(".content_fijo_dth").css('display', 'block');

                        $(".content_adicionales").css('display', 'none');

                    } else if (ventas == "FIJO ADICIONALES") {

                        $(".content_fijo_bidirecional").css('display', 'none');

                        $(".content_fijo_dth").css('display', 'none');

                        $(".content_adicionales").css('display', 'block');
                    }
                });
                $('#plan_movil').change(function () {

                    var plan = $('#plan_movil').val();

                    if (plan == "") {

                        $(".plan_movil_prepago").css('display', 'none');

                        $(".plan_movil_pospago").css('display', 'none');

                    } else if (plan == "POSPAGO") {

                        $(".plan_movil_prepago").css('display', 'none');

                        $(".plan_movil_pospago").css('display', 'block');

                    } else if (plan == "PREPAGO") {

                        $(".plan_movil_prepago").css('display', 'block');

                        $(".plan_movil_pospago").css('display', 'none');

                    }

                });
                //datosventa prepago
                $('#movil_prepago').change(function () {

                    var plan_m = $('#plan_movil').val();
                    var movil_pre = $('#movil_prepago').val();

                    $('#movil_pospago option:first-child').attr("selected", "selected");

                    $('#movil_pospago')[0].selectedIndex = 0;   

                    if (movil_pre == "" && plan_m == "PREPAGO") {

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'none');

                        $("#div_row10").css('display', 'none');

                        $("#div_row11").css('display', 'none');

                         $("#ocultar_btn").css('display', 'none');

                    } else if (movil_pre == "PORTACION" && plan_m == "PREPAGO") { 

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'block');

                        $("#div_row5").css('display', 'block');

                        $("#div_row6").css('display', 'block');

                        $("#div_row7").css('display', 'block');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'block');

                        $("#div_row10").css('display', 'none');

                        $("#div_row11").css('display', 'block');

                        $("#ocultar_btn").css('display', 'none');


                    } else if (movil_pre == "LINEA NUEVA (WB)" && plan_m == "PREPAGO") {

                       
                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'block');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'block');

                        $("#div_row10").css('display', 'block');

                        $("#div_row11").css('display', 'none');

                         $("#ocultar_btn").css('display', 'none');

                    }

                });

                //datos venta pospago 
                $('#movil_pospago').change(function () {

                    var plan_m = $('#plan_movil').val();
                    var movil_pos = $('#movil_pospago').val();

                    $('#movil_prepago option:first-child').attr("selected", "selected");

                    $('#movil_prepago')[0].selectedIndex = 0;

                    if (movil_pos == "" && plan_m == "POSPAGO") {

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'none');

                        $("#div_row11").css('display', 'none');

                         $("#ocultar_btn").css('display', 'none');

                    } else if (movil_pos == "PORTACION" && plan_m == "POSPAGO") {

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'block');

                        $("#div_row5").css('display', 'block');

                        $("#div_row6").css('display', 'block');

                        $("#div_row7").css('display', 'block');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'block');

                         $("#div_row11").css('display', 'block');

                          $("#ocultar_btn").css('display', 'none');


                    } else if (movil_pos == "LINEA NUEVA" && plan_m == "POSPAGO") {

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'block');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'none');

                        $("#div_row9").css('display', 'block');

                        $("#div_row11").css('display', 'none');

                         $("#ocultar_btn").css('display', 'none');

                    } else if (movil_pos == "MIGARCION") {

                        $("#div_row1").css('display', 'none');

                        $("#div_row2").css('display', 'none');

                        $("#div_row3").css('display', 'none');

                        $("#div_row4").css('display', 'none');

                        $("#div_row5").css('display', 'none');

                        $("#div_row6").css('display', 'none');

                        $("#div_row7").css('display', 'none');

                        $("#div_row8").css('display', 'block');

                        $("#div_row9").css('display', 'block');

                        $("#div_row11").css('display', 'none'); 
                        
                    }

                });

                $('#consultar_iccid').click(function () {
                
                    $("#ocultar_btn").css('display', 'block');
               
                 });


                    //validaciones
           $('#btn_guardar').click(function (){

                //fecha venta
                var date_fecha = $('#date_fecha').val();

                //fijo o movil
                var tipo_servicio = $('#tipo_servicio').val();

                //fijo bidirecional, fijo dth, fijo adicionales
                var tipo_venta = $('#tipo_venta').val();

                //fijo bidirecional
                var fijo_television1 = $('#fijo_television1').val();
                var fijo_internet1 = $('#fijo_internet1').val();
                var fijo_telefonia1 = $('#fijo_telefonia1').val();
                var fijo_observaciones1 = $('#fijo_observaciones1').val();

                //fijo dth
                var fijo_television2 = $('#fijo_television2').val();
                var fijo_internet2 = $('#fijo_internet2').val();
                var fijo_telefonia2 = $('#fijo_telefonia2').val();
                var fijo_observaciones2 = $('#fijo_observaciones2').val();

                //fijo adicionales
                var adicionales_tv = $('#adicionales_tv').val();
                var adici_observaciones = $('#adici_observaciones').val();

                //prepago o pospago
                var plan_movil = $('#plan_movil').val();

                // pospago 
                var movil_pospago = $('#movil_pospago').val();
                var movil_plan = $('#movil_plan').val();
                var pospago_observac = $('#pospago_observac').val();
                //Prepago 
                var movil_prepago = $('#movil_prepago').val();
                var movil_recarga = $('#movil_recarga').val();
                var movil_transaccion = $('#movil_transaccion').val();
                var movil_sim = $('#movil_sim').val();
                var prepago_observac = $('#prepago_observac').val();

                //cliente
                var nombre_cliente = $('#nombre_cliente').val();
                var tipo_doc_cliente = $('#tipo_doc_cliente').val();
                var doc_cliente = $('#doc_cliente').val();
                var fecha_exp_cliente = $('#fecha_exp_cliente').val();
                var direccion_cliente = $('#direccion_cliente').val();
                var ciudad_cliente = $('#ciudad_cliente').val();
                var numero_cliente = $('#numero_cliente').val();
                var correo_cliente = $('#correo_cliente').val();
                 var barrio_cliente = $('#barrio_cliente').val();
                //cliente1

                var nombre_cliente1 = $('#nombre_cliente1').val();
                var tipo_doc_cliente1 = $('#tipo_doc_cliente1').val();
                var doc_cliente1 = $('#doc_cliente1').val();
                var fecha_exp_cliente1 = $('#fecha_exp_cliente1').val();
                var direccion_cliente1 = $('#direccion_cliente1').val();
                var ciudad_cliente1 = $('#ciudad_cliente1').val();
                var numero_cliente1 = $('#numero_cliente1').val();
                var correo_cliente1 = $('#correo_cliente1').val();
                var barrio_cliente1 = $('#barrio_cliente1').val();

                //asesor

                var id_asesorr = $('#id_asesorr').val();
                var tropa_asesor = $('#tropa_asesor').val();
                var supervisor_asesor = $('#supervisor_asesor').val();

                //portacion

                var min_temporal = $('#min_temporal').val();
                var venta_op_donante = $('#venta_op_donante').val();
                var min_definitivo = $('#min_definitivo').val();
                var venta_ni = $('#venta_ni').val();
                var id_generado_sistema = $('#id_generado_sistema').val();
                var plataforma = $('#plataforma').val();
                var venta_iccid = $('#venta_iccid').val(); 

                var min = $('#min').val(); 

                var min_a_emigrar = $('#min_a_emigrar').val();

                //fijo
                var venta_cuenta = $('#venta_cuenta').val();
                var venta_ot = $('#venta_ot').val();

                var validacion_iccid_1 = $('#validacion_iccid_1').val();
                var required_iccid_1 = $('#required_iccid_1').val();
                
                 if (tipo_servicio=='FIJO') {

                     if (tipo_venta=='FIJO BIDIRECCIONAL') {

                        //-bidemencional

                    $("#fijo_television1").attr('required', true);

                    $("#fijo_internet1").attr('required', true);

                    $("#fijo_telefonia1").attr('required', true);

                    $("#fijo_observaciones1").attr('required', true);


                    //-fijo dth 

                    $("#fijo_television2").attr('required', false);

                    $("#fijo_internet2").attr('required', false);

                    $("#fijo_telefonia2").attr('required', false);

                    $("#fijo_observaciones2").attr('required', false);


                    //-adcicionales

                    $("#adicionales_tv").attr('required', false);

                    $("#adici_observaciones").attr('required', false);

                    //-prepago

                    $("#movil_prepago").attr('required', false);

                    $("#movil_recarga").attr('required', false);

                    $("#movil_sim").attr('required', false);

                    $("#prepago_observac").attr('required', false);

                    //-pospago

                    $("#movil_pospago").attr('required', false);

                    $("#movil_plan").attr('required', false);
 
                    $("#pospago_observac").attr('required', false);

                    // fecha_exp

                    


                    }else if (tipo_venta=='FIJO DTH') {

                    //-bidemencional

                    $("#fijo_television1").attr('required', false);

                    $("#fijo_internet1").attr('required', false);

                    $("#fijo_telefonia1").attr('required', false);

                    $("#fijo_observaciones1").attr('required', false);


                    //-fijo dth 

                    $("#fijo_television2").attr('required', true);

                    $("#fijo_internet2").attr('required', true);

                    $("#fijo_telefonia2").attr('required', true);

                    $("#fijo_observaciones2").attr('required', true);


                    //-adcicionales

                    $("#adicionales_tv").attr('required', false);

                    $("#adici_observaciones").attr('required', false);

                    //-prepago

                    $("#movil_prepago").attr('required', false);

                    $("#movil_recarga").attr('required', false);

                    $("#movil_sim").attr('required', false);

                    $("#prepago_observac").attr('required', false);

                    //-pospago

                    $("#movil_pospago").attr('required', false);

                    $("#movil_plan").attr('required', false);
 
                    $("#pospago_observac").attr('required', false);

                    // fecha_exp 
                     
                          
                    }else if (tipo_venta=='FIJO ADICIONALES') {

                    //-bidemencional

                    $("#fijo_television1").attr('required', false);

                    $("#fijo_internet1").attr('required', false);

                    $("#fijo_telefonia1").attr('required', false);

                    $("#fijo_observaciones1").attr('required', false);


                    //-fijo dth 

                    $("#fijo_television2").attr('required', false);

                    $("#fijo_internet2").attr('required', false);

                    $("#fijo_telefonia2").attr('required', false);

                    $("#fijo_observaciones2").attr('required', false);


                    //-adcicionales

                    $("#adicionales_tv").attr('required', true);

                    $("#adici_observaciones").attr('required', true);

                    //-prepago

                    $("#movil_prepago").attr('required', false);

                    $("#movil_recarga").attr('required', false);

                    $("#movil_sim").attr('required', false);

                    $("#prepago_observac").attr('required', false);

                    //-pospago

                    $("#movil_pospago").attr('required', false);

                    $("#movil_plan").attr('required', false);
 
                    $("#pospago_observac").attr('required', false);

                    // fecha_exp 
                     
                    }

                }
                else if (tipo_servicio=='MOVIL') {

                   if (plan_movil=="POSPAGO") {

                    //-bidemencional

                    $("#fijo_television1").attr('required', false);

                    $("#fijo_internet1").attr('required', false);

                    $("#fijo_telefonia1").attr('required', false);

                    $("#fijo_observaciones1").attr('required', false);


                    //-fijo dth 

                    $("#fijo_television2").attr('required', false);

                    $("#fijo_internet2").attr('required', false);

                    $("#fijo_telefonia2").attr('required', false);

                    $("#fijo_observaciones2").attr('required', false);


                    //-adcicionales

                    $("#adicionales_tv").attr('required', false);

                    $("#adici_observaciones").attr('required', false);

                    //-prepago

                    $("#movil_prepago").attr('required', false);

                    $("#movil_recarga").attr('required', false);

                    $("#movil_sim").attr('required', false);

                    $("#prepago_observac").attr('required', false);

                    //-pospago

                    $("#movil_pospago").attr('required', true);

                    $("#movil_plan").attr('required', true);
 
                    $("#pospago_observac").attr('required', true);

                    // fecha_exp 
                     
                    
                    }else if (plan_movil=="PREPAGO") {

                    //-bidemencional

                    $("#fijo_television1").attr('required', false);

                    $("#fijo_internet1").attr('required', false);

                    $("#fijo_telefonia1").attr('required', false);

                    $("#fijo_observaciones1").attr('required', false);


                    //-fijo dth 

                    $("#fijo_television2").attr('required', false);

                    $("#fijo_internet2").attr('required', false);

                    $("#fijo_telefonia2").attr('required', false);

                    $("#fijo_observaciones2").attr('required', false);


                    //-adcicionales

                    $("#adicionales_tv").attr('required', false);

                    $("#adici_observaciones").attr('required', false);

                    //-prepago

                    $("#movil_prepago").attr('required', true);           

                    $("#movil_sim").attr('required', true);

                    $("#prepago_observac").attr('required', true);

                    //-pospago

                    $("#movil_pospago").attr('required', false);

                    $("#movil_plan").attr('required', false);
 
                    $("#pospago_observac").attr('required', false);

                   
                    if (movil_recarga!="") {

                        $("#movil_transaccion").attr('required', true);

                    }

                    }
                }
                else if (tipo_servicio=="FIJO") {

                        $("#venta_cuenta").attr('required', true);
                        $("#venta_ot").attr('required', true);
                        $("#min").attr('required', false);
                        $("#min_temporal").attr('required', false);
                        $("#venta_op_donante").attr('required', false);
                        $("#min_definitivo").attr('required', false);
                        $("#venta_ni").attr('required', false);
                        $("#id_generado_sistema").attr('required', false);
                        $("#plataforma").attr('required', false); 
                        $("#min_a_emigrar").attr('required', false);
                        $("#venta_iccid").attr('required', false);
                   
                }else if ((tipo_servicio=="MOVIL" && movil_prepago=="PORTACION") || (tipo_servicio=="MOVIL" && movil_pospago=="PORTACION")) {

                       $("#venta_cuenta").attr('required', false);
                       $("#venta_ot").attr('required', false);
                       $("#min").attr('required', false);
                       $("#min_temporal").attr('required', true);
                       $("#venta_op_donante").attr('required', true);
                       $("#min_definitivo").attr('required', true);
                       $("#venta_ni").attr('required', true);
                       $("#id_generado_sistema").attr('required', true);
                       $("#plataforma").attr('required', true); 
                       $("#min_a_emigrar").attr('required', false);
                       $("#venta_iccid").attr('required', true);

                 if (validacion_iccid_1=='0') {


                    $("#required_iccid_1").attr('required', true);
                }else if (validacion_iccid_1=='1') {

                    
                    $("#required_iccid_1").attr('required', false);

                    

                  
                }

                }else if ((tipo_servicio=="MOVIL" && movil_pospago=="LINEA NUEVA") || (tipo_servicio=="MOVIL" && movil_prepago=="LINEA NUEVA (WB)")) {

                        $("#venta_cuenta").attr('required', false);
                        $("#venta_ot").attr('required', false);
                        $("#min").attr('required', true);
                        $("#min_temporal").attr('required', false);
                        $("#venta_op_donante").attr('required', false);
                        $("#min_definitivo").attr('required', false);
                        $("#venta_ni").attr('required', false);
                        $("#plataforma").attr('required', false);
                        $("#id_generado_sistema").attr('required', false);
                        $("#min_a_emigrar").attr('required', false);
                        $("#venta_iccid").attr('required', true);

                     if (validacion_iccid_1=='0') {

                         
                    $("#required_iccid_1").attr('required', true);
                }else if (validacion_iccid_1=='1') {

                    $("#required_iccid_1").attr('required', false);
                    
                }
                     
                }else if (tipo_servicio=="MOVIL" && movil_pospago=="MIGARCION") {

                        $("#venta_cuenta").attr('required', false);
                        $("#venta_ot").attr('required', false);
                        $("#min").attr('required', false);
                        $("#min_temporal").attr('required', false);
                        $("#venta_op_donante").attr('required', false);
                        $("#min_definitivo").attr('required', false);
                        $("#venta_ni").attr('required', false);
                        $("#plataforma").attr('required', false);
                        $("#id_generado_sistema").attr('required', false);
                        $("#min_a_emigrar").attr('required', true);
                        $("#venta_iccid").attr('required', true);
                    
                }else if (tipo_servicio=="MOVIL" && movil_prepago=="LINEA NUEVA (WB)") {

                    $("#movil_sim").attr('required', false);
                  

                }else if(tipo_servicio=="MOVIL" && movil_prepago!="LINEA NUEVA (WB)"){

                    $("#movil_sim").attr('required', true);
                    

                }

                
            });
                    
            });
        

