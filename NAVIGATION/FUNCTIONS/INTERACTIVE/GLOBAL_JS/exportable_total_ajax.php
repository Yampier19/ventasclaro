<?php 

require ('../../../CONNECTION/SECURITY/session.php');  
    require('../../../CONNECTION/SECURITY/conex.php');
     
//session_start();

//Exportar datos de php a Excel
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=total_inventarios.xls");

?>
  <div class="col-md-12">
          <div class="contenedor_de_tabla">
                <div class="table-responsive">
    <table id="usertable" class="table table-striped">
      <thead class="encabezado_Tabla1">
        <tr>
          <th id=""><center>ICCID</center></th>
          <th id=""><center>PORTABILIDAD</center></th>
          <th id=""><center>ID ASESOR</center></th> 
          <th id=""><center>NOMBRE ASESOR</center></th>
          <th id=""><center>NOMBRE TROPA</center></th>
          <th id=""><center>NOMBRE SUPERVISOR</center></th>
          <th id=""><center>NUMERO BLOQUE</center></th>
          <th id=""><center>ESTADO</center></th>
          <th id=""><center>FECHA ASIGNACION</center></th>
          <th id=""><center>ESTADO VENTA</center></th>
          <th id=""><center>FECHA REGISTRO</center></th>
        </tr>
      </thead>
      <tbody id="letra_a">
  <?php   
  $select_inventarios = mysqli_query($conex,"SELECT iccid, portabilidad, fk_asesor, nombre_asesor, nombre_tropa, nombre_supervisor, numero_bloque, estado AS estado, fecha_asignacion, estado_venta, fecha_registro FROM inventarios; "); 

    while ($inventario =(mysqli_fetch_array($select_inventarios))) {
         $iccid = $inventario['iccid'];
         $portabilidad = $inventario['portabilidad'];
         $fk_asesor = $inventario['fk_asesor']; 
         $nombre_asesor = $inventario['nombre_asesor'];
         $nombre_tropa = $inventario['nombre_tropa'];
         $nombre_supervisor = $inventario['nombre_supervisor'];
         $numero_bloque = $inventario['numero_bloque'];
         $estado = $inventario['estado'];
         $fecha_asignacion = $inventario['fecha_asignacion'];
         $estado_venta = $inventario['estado_venta'];
         $fecha_registro = $inventario['fecha_registro'];

   echo "<tr role='row'>
         <td><span class='descripcion'>'".$iccid."</span></td>
         <td><span class='descripcion'>".$portabilidad."</span></td>
         <td><span class='descripcion'>".$fk_asesor."</span></td>
         <td><span class='descripcion'>".$nombre_asesor."</span></td>
         <td><span class='descripcion'><p class='espacio'>".$nombre_tropa."</p></span></td>
         <td><span class='descripcion'>".$nombre_supervisor."</span></td>
         <td><span class='descripcion'>".$numero_bloque."</span></td>
         <td><span class='descripcion'>".$estado."</span></td>
         <td><span class='descripcion'>".$fecha_asignacion."</span></td>
         <td><span class='descripcion'>".$estado_venta."</span></td>
         <td><span class='descripcion'>".$fecha_registro."</span></td>
    </tr>"; 
       }
    ?>
  </tbody>

</table>

</div>
</div>
<?php //CONSULTA 1 ?>
</div>

 