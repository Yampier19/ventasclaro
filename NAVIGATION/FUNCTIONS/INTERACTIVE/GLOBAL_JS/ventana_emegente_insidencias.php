<?php
require ('../../../CONNECTION/SECURITY/session.php');

if ($user_name != '') { ?>

<!DOCTYPE html>
<html>
<head>
	<title>Ventas Claro</title>
	        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script type="text/javascript" src="../../../DESIGN/JS/formulario_js/ajax_popper_bostrap.js"></script>
    <script type="text/javascript" src="../../../DESIGN/JS/formulario_js/boostrap.js"></script>
    <link href="../../../DESIGN/CSS/boostrap_formulario/boostrap_min.css" rel="stylesheet"/>
</head>
<style type="text/css">
	.card-header{
		background: rgb(239, 56, 41) !important;
		color: white;
		border-style:double;
		box-shadow:2px 2px 2px;
	}
	#ajustar_style{
		margin-left:5%; 
		margin-right:5%;
		margin-top:5%;
		margin-bottom:5%;
	}
	  .asterisco_obligatorio{
            color: red;
        }
</style>
<body>

<?php 
if (isset($_GET['xdfghx'])) {

    $iccid = base64_decode($_GET['xdfghx']);
    
} ?>

<div class="card" id="ajustar_style">
  <div class="card-header">
    Reportar Insidencia
  </div>
  <div class="card-body">
  	<div class="form-group row">
  	<div class="col-md-6">
  		<label>Iccid</label>
  	</div>
  	<div class="col-md-6">
  		<p class="form-control"><?php echo $iccid; ?></p>
  	</div>	
  	</div>
  	<div class="form-group row">
  		<div class="col-md-6">
  			<label>Tipo de insidencia <span class="asterisco_obligatorio">*</span> </label>
  		</div>
  		<div class="col-md-6">
  			<input  type="text" class="form-control" name="tipo_insidencia" id="tipo_insidencia">
  		</div>
  	</div>

  	<div class="form-group row">
  		<div class="col-md-6">
  			<label>Cambio de estado a: <span class="asterisco_obligatorio">*</span> </label>
  		</div>
  		<div class="col-md-6">
  			<input  type="text" class="form-control" name="camnbio_estado" id="camnbio_estado">
  		</div>
  	</div>

  	<div class="form-group row">
  		<div class="col-md-6">
  			<label>Observacion de insidencia <span class="asterisco_obligatorio">*</span> </label>
  	    </div>
  	    <div class="col-md-6">
  	    	<input type="text" class="form-control" name="observaciones_insidencia" id="observaciones_insidencia">
  	    </div>
    </div>
 <center><button type="button" class="btn btn-danger">Danger</button></center>
  </div>
</div>


</body>
</html>

<?php
}else {
echo 'Usted no tiene los permisos necesarios para acceder a esta informacion' . $user_name;
}

?>