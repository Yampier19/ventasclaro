-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-10-2019 a las 23:49:59
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventasclaro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adicionales`
--

CREATE TABLE `adicionales` (
  `id_adicionales` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fecha_venta` date NOT NULL,
  `canales_adicionales` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asesor`
--

CREATE TABLE `asesor` (
  `id_asesor` int(11) NOT NULL,
  `nombres` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `numero_documento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `codigo_asesor` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `tropa` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `supervisor` varchar(20) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `asesor`
--

INSERT INTO `asesor` (`id_asesor`, `nombres`, `apellidos`, `numero_documento`, `codigo_asesor`, `tropa`, `supervisor`) VALUES
(1, 'Brayan', 'Galvis', '', '', '', ''),
(2, 'Monica', 'Arevalo Perez', '', '', '', ''),
(3, 'Mabel', 'Yamguma Tovar', '', '', '', ''),
(4, 'Mayra', 'Meza Garcia', '', '', '', ''),
(5, 'Michael', 'Castellanos Baron', '', '', '', ''),
(6, 'Elizabeth ', 'Diaz Portela', '', '', '', ''),
(7, 'Julieth Dayana', 'Duarte Wilches', '', '', '', ''),
(8, 'Orlanis Mariela', 'Riveira Guzman', '', '', '', ''),
(9, 'Luisa Fernanda', 'Figueredo Velasco', '', '', '', ''),
(10, 'Yuleidy Patricia', 'Cruz Mongui', '', '', '', ''),
(11, 'Aixa Patricia', 'Renteria Ferreira', '', '', '', ''),
(12, 'Sandra Patricia', 'Rivera Martinez', '', '', '', ''),
(13, 'Brayan orlando Galvis jaimes', '1000250137', '', '', '', ''),
(14, 'Monica Arevalo Perez', '1094167169', '', '', '', ''),
(15, 'Mabel Natalia Yanguma Tovar', '1023034903', '', '', '', ''),
(16, 'Mayra  Alexandra Meza  Garcia', '1024581951', '', '', '', ''),
(17, 'Michael Castellanos Baron', '1022377067', '', '', '', ''),
(18, 'Elizabeth Diaz Portela', '1006003994', '', '', '', ''),
(19, 'Julieth Dayana Duarte Wilches', '53153082', '', '', '', ''),
(20, 'Orlanis Mariela Riveira Guzman', '49608584', '', '', '', ''),
(21, 'Luisa Fernanda  Figueredo Velasco', '1000619121', '', '', '', ''),
(22, 'Yuleidy Patricia Cruz Mongui', '1033748249', '', '', '', ''),
(23, 'Aixa Carolina Renteria Ferreira', '103249183', '', '', '', ''),
(24, 'Sandra Patricia Rivera Martinez', '52861412', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `ciudad` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `activo` varchar(45) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `id_departamento`, `ciudad`, `activo`) VALUES
(1, 1, 'Bogota', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_info_cliente` int(11) NOT NULL,
  `fk_prepago` int(11) NOT NULL,
  `fk_pospago` int(11) NOT NULL,
  `fk_triple_play` int(11) NOT NULL,
  `fk_adicionales` int(11) NOT NULL,
  `fk_datos_venta` int(11) NOT NULL,
  `fk_asesor` int(11) NOT NULL,
  `fk_ciudad` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `nombre_completo` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `tipo_documento` char(5) COLLATE latin1_spanish_ci NOT NULL,
  `numero_documento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_expedicion` date NOT NULL,
  `direccion` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `ciudad` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `numero_contacto` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `correo` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `tropa` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `supervisor` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `doc_supervisor` varchar(20) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_info_cliente`, `fk_prepago`, `fk_pospago`, `fk_triple_play`, `fk_adicionales`, `fk_datos_venta`, `fk_asesor`, `fk_ciudad`, `fk_user`, `nombre_completo`, `tipo_documento`, `numero_documento`, `fecha_expedicion`, `direccion`, `ciudad`, `numero_contacto`, `correo`, `tropa`, `supervisor`, `doc_supervisor`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 2, 'Jorge Romero', 'TI', '79975858', '0198-10-12', ' Barrio Juan Rey', '1', '3208025636', '123ls@gmail.com', 'MAY.1101729 ', 'Mayra Alejandra Sali', '1019086804'),
(2, 0, 0, 0, 0, 0, 0, 0, 2, 'Edgarmery Barreto', 'CC', '23966121', '2012-10-10', 'Cra 80#71C-28 SUR - Bosa naranjos', '1', '3208088552', 'kjhfsiknkhiknn@gmail', 'MAY.1101728', 'Mayra Alejandra Sali', '1019086804'),
(3, 0, 0, 0, 0, 0, 0, 0, 2, 'Jose Omar Garcia ', 'CC', '79562303', '2000-02-12', 'Las Lomaa', '1', '3203417006', 'hgdj@gmail.com', 'MAY.1101729 ', 'Mayra Alejandra Sali', '1019086804'),
(4, 0, 0, 0, 0, 0, 0, 0, 2, 'Danilo Cely	', 'CC', '79579732		', '1998-03-12', 'Cra 13#11-05 Centro		', '1', '3203432160		', 'gafd@gmail.com', 'MAY.1101729 ', 'Mayra Alejandra Sali', '1019086804'),
(5, 0, 0, 0, 0, 0, 0, 0, 2, 'Oscar Lopez	', 'CC', '79370236		', '2011-12-10', 'Centro		', '1', '3203494528		', 'HJFAKS@GAMIL.COM', 'MAY.1101729 ', 'Mayra Alejandra Sali', '1019086804');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_venta`
--

CREATE TABLE `datos_venta` (
  `id_datos_venta` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `iccid` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `nip` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `operador_donante` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `cuenta` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `ot` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `min_c` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `min_temporal` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `min_definitivo` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `min_a_emigrar` varchar(45) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `datos_venta`
--

INSERT INTO `datos_venta` (`id_datos_venta`, `fk_user`, `iccid`, `nip`, `operador_donante`, `cuenta`, `ot`, `min_c`, `min_temporal`, `min_definitivo`, `min_a_emigrar`) VALUES
(1, 2, '502204860224', '70049', 'virgin', 'N/A', 'N/A', 'N/A', '3208025636', '3142805951', 'N/A'),
(2, 2, '502204860209	', '23234', 'Movistar', 'N/A', 'N/A', 'N/A', '3208088552', '3186394841', 'N/A'),
(3, 2, '502204743034', '17488		', 'virgin		', 'N/A', 'N/A', 'N/A', '3203417006		', '3137801450		', 'N/A'),
(4, 2, '502204743032', '10129		', 'Tigo		', 'N/A', 'N/A', 'N/A', '3203432160		', '3503388210		', 'N/A'),
(5, 2, '502206616007	', 'N/A', 'N/A', 'N/A', 'N/A', '3203494528		', 'N/A', 'N/A', 'N/A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pospago`
--

CREATE TABLE `pospago` (
  `id_pospago` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fecha_venta` date NOT NULL,
  `pospago` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `plan` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prepago`
--

CREATE TABLE `prepago` (
  `id_prepago` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fecha_venta` date NOT NULL,
  `prepago` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `recargar` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `costo_sim` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `prepago`
--

INSERT INTO `prepago` (`id_prepago`, `fk_id_user`, `fecha_venta`, `prepago`, `recargar`, `costo_sim`, `observaciones`, `fecha_registro`) VALUES
(1, 2, '2019-10-24', 'PORTACION', '6000', '1000', 'Paquete de 6 dias minutos ilimitados y 170 MB', '2019-10-24 16:32:18'),
(2, 2, '0000-00-00', 'PORTACION', '6000', '1000', '6 DÍAS X $6.000 y 170 mb', '2019-10-24 16:37:20'),
(3, 2, '2019-10-24', 'PORTACION', '6000', '1000', '6 DÍAS X $6.000 y 170 mb', '2019-10-24 16:40:32'),
(4, 2, '2019-10-24', 'PORTACION', '6000', '1000', '6 DÍAS X $6.000 170mb', '2019-10-24 16:44:30'),
(5, 2, '0000-00-00', 'LINEA NUEVA (WB)', '6000', '1000', '6 DÍAS X $6.000 Y 170 mb', '2019-10-24 16:47:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roleslogin`
--

CREATE TABLE `roleslogin` (
  `id_rol` int(11) NOT NULL,
  `nombre_rol` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `activo` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `triple_play`
--

CREATE TABLE `triple_play` (
  `id_triple_play` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `oferta_fijo` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_venta` date NOT NULL,
  `television` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `internet` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `telefonia` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `names` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `surnames` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `correo` varchar(60) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userlogin`
--

CREATE TABLE `userlogin` (
  `id_log` int(11) NOT NULL,
  `name_user` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(80) COLLATE latin1_spanish_ci NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_loginrol` int(3) NOT NULL,
  `activo` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `userlogin`
--

INSERT INTO `userlogin` (`id_log`, `name_user`, `password`, `id_user`, `id_loginrol`, `activo`) VALUES
(1, 'admin', 'ODFkYzliZGI1MmQwNGRjMjAwMzZkYmQ4MzEzZWQwNTU=', 1, 1, 1),
(2, 'asesor', 'ODFkYzliZGI1MmQwNGRjMjAwMzZkYmQ4MzEzZWQwNTU=', 2, 3, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adicionales`
--
ALTER TABLE `adicionales`
  ADD PRIMARY KEY (`id_adicionales`);

--
-- Indices de la tabla `asesor`
--
ALTER TABLE `asesor`
  ADD PRIMARY KEY (`id_asesor`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_info_cliente`);

--
-- Indices de la tabla `datos_venta`
--
ALTER TABLE `datos_venta`
  ADD PRIMARY KEY (`id_datos_venta`);

--
-- Indices de la tabla `pospago`
--
ALTER TABLE `pospago`
  ADD PRIMARY KEY (`id_pospago`);

--
-- Indices de la tabla `prepago`
--
ALTER TABLE `prepago`
  ADD PRIMARY KEY (`id_prepago`);

--
-- Indices de la tabla `roleslogin`
--
ALTER TABLE `roleslogin`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `triple_play`
--
ALTER TABLE `triple_play`
  ADD PRIMARY KEY (`id_triple_play`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adicionales`
--
ALTER TABLE `adicionales`
  MODIFY `id_adicionales` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `asesor`
--
ALTER TABLE `asesor`
  MODIFY `id_asesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_info_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `datos_venta`
--
ALTER TABLE `datos_venta`
  MODIFY `id_datos_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pospago`
--
ALTER TABLE `pospago`
  MODIFY `id_pospago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prepago`
--
ALTER TABLE `prepago`
  MODIFY `id_prepago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roleslogin`
--
ALTER TABLE `roleslogin`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `triple_play`
--
ALTER TABLE `triple_play`
  MODIFY `id_triple_play` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userlogin`
--
ALTER TABLE `userlogin`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
